$(document).ready(function() {
	$(".loadScreen").append('<div class="loadingBlock"></div>');
	$(".loadingBlock").append('<img src="../img/gif/loadImage.gif" alt="Caricamento in corso" width="1871" height="1871" id="ajaxImage">');
	$(".modalImage").on('click tap', function(event) {
		$(".maxModalScreen").css('visibility', 'hidden');
	});
	$(".buttonSelected").on('click tap', function(event) {
		$(".maxModalScreen").css('visibility', 'hidden');
	});
		$("input[data-user]").on('click tap', function(event) {
			event.preventDefault();
			data = $(this).attr('data-user');
	    	$(".loadingBlock").css("display","block");
	    	$.ajax({
	    		url: "functionPages/functionRemoveUser.php",
	        	type: 'POST',
	        	data: data,
	        	async: true,
		       	success: function (res) {
		       		$(".loadingBlock").css("display","none");
		       		$("#modalScreenAccept").css('visibility', 'visible');
		       	},
		       	error: function (xhr, ajaxOptions, thrownError) {
		       		console.warn(xhr.status);
		        	console.warn(thrownError);
		        	$("#modalScreenError").css('visibility', 'visible');
		      	}	
	  		});
		});
});