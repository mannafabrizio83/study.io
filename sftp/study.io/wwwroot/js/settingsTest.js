
$(".loadScreen").append('<div class="loadingBlock"></div>');
$(".loadingBlock").append('<img src="../img/gif/loadImage.gif" alt="Caricamento in corso" width="1871" height="1871" id="ajaxImage">');
$(".buttonSelected").on('click tap', function(event) {
	$(".maxModalScreen").css('visibility', 'hidden');
});
$("form").on('submit', function(event) {
	event.preventDefault();
	$(".loadingBlock").css("display","block");
	$.ajax({
  		url: $("form").attr('action'),
  		type: 'POST',
  		data: $("form").serialize(),
   		async: true,
   		success: function (res) {
   			$(".loadingBlock").css("display","none");
   			$("#modalScreenAccept").css('visibility', 'visible');
   		},
   		error: function (xhr, ajaxOptions, thrownError) {
  			console.warn(xhr.status);
       		console.warn(thrownError);
       		$(".loadingBlock").css("display","none");
      		$("#modalScreenError").css('visibility', 'visible');
   		}	
  	});
});	