$(document).ready(function(){
	$(".loadScreen").append('<div class="loadingBlock"></div>');
  $(".loadingBlock").append('<img src="../img/gif/loadImage.gif" alt="Caricamento in corso" width="1871" height="1871" id="ajaxImage">');
  $(".buttonSelected").on('click tap', function(event) {
    $(".maxModalScreen").css('visibility', 'hidden');
  });
  $(".modalImage").on('click tap', function(event) {
    $(".maxModalScreen").css('visibility', 'hidden');
    $(".loadingBlock").css("display","none");
  });
  $("input[value=Chiudi]").on('click tap', function(){
    $(".maxModalScreen").css('visibility', 'hidden');
    $(".loadingBlock").css("display","none");
  });
	$("select").on('change', function(event){
  	event.preventDefault();
  	let dataResp = $("form")[0];
  	// console.log(dataResp);
  	let data = new FormData(dataResp);
    	$.ajax({
      	url: $("form").attr('action'),
      	type: 'POST',
      	data: data,
     	 	contentType: false,
      	cache: false,
      	processData: false,
      	success: function (res) {
        	let data = JSON.parse(res);
						$("[data-load]").html("");
						if(data){
							console.log(data);
							if(data.length > 0){
								for (i=0; i <= data.length;i+=4){
									let appendsData = '<div class="flexRow flex flexBetween marginTopButton row"><div class="col-3-12 center"><span class="uppercase bold"><span class="uppercase bold">'+data[i].coddomanda+'</span></div>	<div class="col-3-12 center">	<span class="uppercase bold">'+data[i].nome+'</span></div><div class="col-3-12 center"><span class="uppercase bold">'+data[i].domanda+'</span></div><div class="col-3-12 center"><span class="uppercase bold"><svg class="svgSetting" width="80" height="80"><use xlink:href="#crossPlus"/></svg></span></div></div><hr>';
									$("[data-load]").append(appendsData);
								}
							}
						}
						else{
							console.log("Nessun Dato");
						}
      	},
      	error: function (xhr, ajaxOptions, thrownError) {
        	console.warn(xhr.status);
        	console.warn(thrownError);
        	$("#modalScreenError").css('visibility', 'visible');
      	}	        
  		});
	});
	$(".searchBar").on('keyup', function(event){
		event.preventDefault();
  	if ( $(this).val().length !== 100 ){
   	 let dataResp = $("form")[0];
    	// console.log(dataResp);
    	let data = new FormData(dataResp);
      	$.ajax({
      	  url: $("form").attr('action'),
        	type: 'POST',
        	data: data,
        	contentType: false,
        	cache: false,
        	processData: false,
        	success: function (res) {
						let data = JSON.parse(res);
						$("[data-load]").html("");
						if(data){
							console.log(data);
							if(data.length > 0){
								for (i=0; i <= data.length;i+=4){
									let appendsData = '<div class="flexRow flex flexBetween marginTopButton row"><div class="col-3-12 center"><span class="uppercase bold"><span class="uppercase bold">'+data[i].coddomanda+'</span></div>	<div class="col-3-12 center">	<span class="uppercase bold">'+data[i].nome+'</span></div><div class="col-3-12 center"><span class="uppercase bold">'+data[i].domanda+'</span></div><div class="col-3-12 center"><span class="uppercase bold">Lorem ipsum</span></div></div><hr>';
									$("[data-load]").append(appendsData);
								}
							}
						}
						else{
							console.log("Nessun Dato");
						}
        	},
        	error: function (xhr, ajaxOptions, thrownError) {
          	console.warn(xhr.status);
          	console.warn(thrownError);
          	$("#modalScreenError").css('visibility', 'visible');
        	}	        
  	  	});
  	}
 	});
});