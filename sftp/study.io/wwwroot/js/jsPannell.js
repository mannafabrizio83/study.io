var date = new Date();
const cal = {
	day: date.getDate(),
	mount: date.getMonth(),
	year: date.getFullYear(),
	createCalendar: function(){
		let thisDay = this.day + "/" + this.mount + "/" + this.year;
		$("#dateInfoUser").text(thisDay);
	}
};
var clock = {
	clock: function(){
		let clockObj = new Date();
		let clock = clockObj.getHours() + ":" + clockObj.getMinutes();
		$("#clockInfoUser").text(clock);
	}
};
$(document).ready(function() {
/*Programmazione Orologio e Calendario*/
	cal.createCalendar();
	setInterval("clock.clock()", 1000);
//-------------------------

    	$(".loadingBlock").css("display","block");
    	$.ajax({
    		url: "form/dashboard.php",
        	type: 'GET',
        	async: true,
        	success: function (res) {
        		$(".loadingBlock").css("display","none");
        		$(".loadScreen").html(res);
        		// console.log(res);
        	},
  		});


	$("#createUser").on('click tap', function(event) {
		event.preventDefault();
    	$(".loadingBlock").css("display","block");
    	$.ajax({
    		url: "form/createUser.php",
        	type: 'GET',
        	async: true,
        	success: function (res) {
        		$(".loadingBlock").css("display","none");
        		$(".loadScreen").html(res);
        		// console.log(res);
        	},
  		});
	});
  $("#modifyRemoveAllQuestions").on('click tap', function(event) {
		event.preventDefault();
    	$(".loadingBlock").css("display","block");
    	$.ajax({
    		url: "form/searchQuery.php",
        	type: 'GET',
        	async: true,
        	success: function (res) {
        		$(".loadingBlock").css("display","none");
        		$(".loadScreen").html(res);
        		// console.log(res);
        	},
  		});
	});
	$("#dashboard").on('click tap', function(event) {
		event.preventDefault();
    	$(".loadingBlock").css("display","block");
    	$.ajax({
    		url: "form/dashboard.php",
        	type: 'GET',
        	async: true,
        	success: function (res) {
        		$(".loadingBlock").css("display","none");
        		$(".loadScreen").html(res);
        		// console.log(res);
        	},
  		});
	});
	$("#removeUserPage").on('click tap', function(event) {
		event.preventDefault();
    	$(".loadingBlock").css("display","block");
    	$.ajax({
    		url: "form/removeUserPage.php",
        	type: 'GET',
        	async: true,
        	success: function (res) {
        		$(".loadingBlock").css("display","none");
        		$(".loadScreen").html(res);
        		// console.log(res);
        	},
  		});
	});
	$("#listUserLink").on('click tap', function(event) {
		event.preventDefault();
    	$(".loadingBlock").css("display","block");
    	$.ajax({
    		url: "form/listUser.php",
        	type: 'GET',
        	async: true,
        	success: function (res) {
        		$(".loadingBlock").css("display","none");
        		$(".loadScreen").html(res);
        	}
  		});
	});
	$("#createTestDb").on('click tap', function(event) {
		event.preventDefault();
    	$(".loadingBlock").css("display","block");
    	$.ajax({
    		url: "form/createQuery.php",
        	type: 'GET',
        	async: true,
        	success: function (res) {
        		$(".loadingBlock").css("display","none");
        		$(".loadScreen").html(res);
        	}
  		});
	});
	$("#addListCourse").on('click tap', function(event) {
		event.preventDefault();
    	$(".loadingBlock").css("display","block");
    	$.ajax({
    		url: "#",
        	type: 'GET',
        	async: true,
        	success: function (res) {
        		$(".loadingBlock").css("display","none");
        		$(".loadScreen").html(res);
        	}
  		});
	});
	$(".setting").on('click tap', function(event) {
		event.preventDefault();
		$(".loadingBlock").css("display","block");
    	$.ajax({
    		url: "form/SettingsQuery.php",
       		type: 'GET',
       		async: true,
       		success: function (res) {
       			$(".loadingBlock").css("display","none");
       			$(".loadScreen").html(res);
       		}        
  		});
	});
	$("input[type=checkbox]").click(function() {
		if ($(this).attr("checked")){
			$(this).removeAttr("checked");
			// console.log("Unchecked");		
		}
		else{
			$(this).attr("checked","true");
			// console.log("checked")
		}
	});
	if($(window).innerWidth() < 980){
		// $(".crossMenu").on('click tap', function(event) {
		// 	event.preventDefault();
		// 	event.stopPropagation();
		// 	$(".rightBar").css('left','0%');
		// 	$(".crossMenu").attr('data-open', 'true');
		// });
		$(".crossMenu").each(function(index, el) {
			$(this).attr('data-open', 'true');
			$(this).on('click tap', function(event) {
				$(".rightBar").css('left','0%');
			});
		});
		$(".centerList").each(function() {
			$(this).attr('data-open', 'false');
			$(this).find(".navbarElement").css("height","0");
			$(this).on('click tap', function(event) {
					$(this).attr("data-open","true");
					$(this).find(".navbarElement").css("height","inherit");
			});
		});
		$("#dashboard").on('click tap',function() {
			$(".hambBox").data('open', 'false');
			$(".rightBar").css('left','-100%');
			$(".loadScreen").css('display', 'block');
		});
		$(".centerList").find(".crossMenu").on('click tap', function(event) {
			event.preventDefault();
			event.stopPropagation();
			$(".centerList").each(function(index, el) {
				$(this).attr('data-open', 'false');
				$(this).find(".navbarElement").css("height","0");
			});
		});
		$(".hambBox").attr('data-open', 'false');
		$(".hambBox").on('click tap', function(event) {
			if($(this).data('open', 'false')){
				$(this).attr('data-open', 'true');
				$(".rightBar").css('left','0%');
				$(".loadScreen").css('display', 'none');
			}
		});	
		$(".setting").on('click tap', function(event) {
			$(".hambBox").data('open', 'false');
			$(".rightBar").css('left','-100%');
			$(".loadScreen").css('display', 'block');
		});
		$(".boxClose").on('click tap', function(event) {
			$(".hambBox").data('open', 'false');
			$(".rightBar").css('left','-100%');
			$(".loadScreen").css('display', 'block');
		});
		$(".listElement .elementListNav").on('click tap', function(event) {
			$(".hambBox").data('open', 'false');
			$(".rightBar").css('left','-100%');
			$(".loadScreen").css('display', 'block');
		});
		$(".setting").on('click tap', function(event) {
			$(".hambBox").data('open', 'false');
			$(".rightBar").css('left','-100%');
			$(".loadScreen").css('display', 'block');
		});
		
		// $(".centerList").find(".buttonList").on('click tap', function(event) {
		// 	$(this).find(".navbarElement").css("height","0");
		// });

	}
	$( window ).on( "orientationchange", function( event ) {
		$(".hambBox").attr('data-open', 'false');
		$(".rightBar").css('left','-100%');
		if( event.target.screen.width <= 980 ){
			$(".hambBox").attr('data-open', 'false');
			$(".hambBox").on('click tap', function(event) {
				if($(this).data('open', 'false')){
					$(this).attr('data-open', 'true');
					$(".rightBar").css('left','0%');
					$(".loadScreen").css('display', 'none');
				}
			});	
			$(".boxClose").on('click tap', function(event) {
				$(".hambBox").data('open', 'false');
				$(".rightBar").css('left','-100%');
				$(".loadScreen").css('display', 'block');
			});
			$(".listElement").on('click tap', function(event) {
				$(".hambBox").data('open', 'false');
				$(".rightBar").css('left','-100%');
				$(".loadScreen").css('display', 'block');
			});
			$(".elementListNav").on('click tap', function(event) {
				$(".hambBox").data('open', 'false');
				$(".rightBar").css('left','-100%');
				$(".loadScreen").css('display', 'block');
			});
			$(".setting").on('click tap', function(event) {
				$(".hambBox").data('open', 'false');
				$(".rightBar").css('left','-100%');
				$(".loadScreen").css('display', 'block');
			});
		}
		else{
			$(".hambBox").on('click tap', function(event) {
				event.stopPropagation();
			});
			$(".listElement").on('click tap', function(event) {
				event.stopPropagation();
				$(".rightBar").css('left','0%');
				$(".loadScreen").css('display', 'none');
			});
			$(".elementListNav").on('click tap', function(event) {
				event.stopPropagation();
				$(".rightBar").css('left','0%');
				$(".loadScreen").css('display', 'none');
			});
			$(".rightBar").css('left','0%');
			$(".loadScreen").css('display', 'none');
		}
	});
});