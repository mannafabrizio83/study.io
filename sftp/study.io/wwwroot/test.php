<?php
		include $_SERVER['DOCUMENT_ROOT'] . '/pages/config/config_db.php';
	  	include $_SERVER['DOCUMENT_ROOT'] . '/pages/function/my_function.php';
	  	include $_SERVER['DOCUMENT_ROOT'] . '/pages/function/us_function.php';

	  	$database = new study_io();
		$conn = $database->connect_db();
		sec_session_start();

		$setting = $database->select_all_id($conn, "settings", "id_settings", 1);

		echo '<center>'.PHP_EOL;

		if($setting->num_rows > 0)
		{
			$setting = $setting->fetch_object();

			echo ' <img class="imgResp" src="/img/images/img-test.png"> '.PHP_EOL;

			//Fisica
			echo '<h2>Test di fisica</h2>'.PHP_EOL;		
			for($i = 1; $i <= $setting->fisica; $i++)
			{
				echo "<b> $i) </b> <br>".PHP_EOL;
			}

			//Matematica -
			echo '<h2>Test di matematica</h2>'.PHP_EOL;
			for($i = 1; $i <= $setting->matematica; $i++)
			{
				$domande = $database->select_all_id($conn, "domande", "codtipodomanda", 2);

				echo "<b> $i) </b> <br>".PHP_EOL;
			}

			//Chimica
			echo '<h2>Test di chimica</h2>'.PHP_EOL;
			for($i = 1; $i <= $setting->chimica; $i++)
			{
				echo "<b> $i) </b> <br>".PHP_EOL;
			}

			//Biologia -
			echo '<h2>Test di biologia</h2>'.PHP_EOL;
			for($i = 1; $i <= $setting->biologia; $i++)
			{
				$domande = $database->select_all_id($conn, "domande", "codtipodomanda", 3);

				echo "<b> $i) </b> <br> <br>".PHP_EOL;
			}

			//Logica
			echo '<h2>Test di logica</h2>'.PHP_EOL;
			for($i = 1; $i <= $setting->logica; $i++)
			{
				echo "<b> $i) </b> <br>".PHP_EOL;
			}

			//Cultura Generale
			echo '<h2>Test di cultura generale</h2>'.PHP_EOL;
			for($i = 1; $i <= $setting->cultura_generale; $i++)
			{
				echo "<b> $i) </b> <br>".PHP_EOL;
			}
		
	 	}
	 	echo '</center>'.PHP_EOL;
?>