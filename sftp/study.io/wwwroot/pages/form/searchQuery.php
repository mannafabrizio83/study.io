<?php
/*-----------------PORTING-LIBRARY-CLASS-----------------*/
	
	include $_SERVER['DOCUMENT_ROOT'] . '/pages/config/config_db.php';
	include $_SERVER['DOCUMENT_ROOT'] . '/pages/function/my_function.php';

/*-------------------------------------------------------*/

	$programm = new study_io;
	$conn = $programm->connect_db();
  $result = $programm->inizializeStampData($conn);
  //print_r($result);
  $subject = $programm->printSubject($conn);
  //print_r($subject);

/*-------------------------------------------------------*/

echo '<form action="functionPages/functionSearchQuery.php" method="post" autocomplete="off" name="searchQueryAll">'.PHP_EOL;
echo '	<div class="screen screenFlex flexColumn">'.PHP_EOL;
echo '		<div class="row">'.PHP_EOL;
echo '			<div class="col-12-12">'.PHP_EOL;
echo '				<h1 class="center uppercase">Ricerca / modifica / rimuovi domanda </h1>'.PHP_EOL;
echo '			</div>'.PHP_EOL;
echo '		</div>'.PHP_EOL;
echo '		<hr>'.PHP_EOL;
echo '		<div class="flexRow flex flexBetween marginTopButton">'.PHP_EOL;
echo '			<div class="col-4-12">'.PHP_EOL;
echo '				<div class="formElement minTopMarg">'.PHP_EOL;
echo '					<label class="uppercase" for="search" class="hidden">Ricerca:</label>'.PHP_EOL;
echo '					<input type="text" id="search"  name="search" required placeholder="Ricerca"  maxlength="100" class="searchBar uppercase">'.PHP_EOL;
//echo '					<input type="hidden" id="searchSend"  name="searchSend"  maxlength="30" class="searchBar uppercase">'.PHP_EOL;
echo '				</div>'.PHP_EOL;
echo '			</div>'.PHP_EOL;
echo '			<div class="col-3-12">'.PHP_EOL;
echo '				<div class="formElement minTopMarg">'.PHP_EOL;
echo '					<label class="uppercase" for="attrSearch">Ricerca per:</label>'.PHP_EOL;
echo '					<select name="attrSearch" id="attrSearch" class="uppercase">'.PHP_EOL;
echo '						<option value="1" class="uppercase">Codice Domanda</option>'.PHP_EOL;
echo '						<option value="2" class="uppercase" selected>Domanda</option>'.PHP_EOL;
echo '					</select>	'.PHP_EOL;
echo '				</div>'.PHP_EOL;
echo '			</div>'.PHP_EOL;
echo '			<div class="col-3-12">'.PHP_EOL;
echo '				<div class="formElement minTopMarg">'.PHP_EOL;
echo '					<label class="uppercase" for="searchMaxNumb">Numero elementi:</label>'.PHP_EOL;
echo '					<select name="searchMaxNumb" id="searchMaxNumb">'.PHP_EOL;
echo '						<option value="25" selected>25</option>'.PHP_EOL;
echo '						<option value="50">50</option>'.PHP_EOL;
echo '						<option value="100">100</option>'.PHP_EOL;
echo '						<option value="150">150</option>'.PHP_EOL;
echo '						<option value="200">200</option>'.PHP_EOL;
echo '						<option value="300">300</option>'.PHP_EOL;
echo '						<option value="400">400</option>'.PHP_EOL;
echo '						<option value="500">500</option>'.PHP_EOL;
echo '					</select>	'.PHP_EOL;
echo '				</div>'.PHP_EOL;
echo '			</div>'.PHP_EOL;
echo '    </div>'.PHP_EOL;
echo '    <hr>'.PHP_EOL;
echo '		<div class="flexRow flex flexBetween marginTopButton">'.PHP_EOL;
echo '			<div class="col-3-12">'.PHP_EOL;
echo '				<div class="formElement minTopMarg">'.PHP_EOL;
echo '					<label class="uppercase" for="subject">Materia:</label>'.PHP_EOL;
echo '					<select name="subject" id="subject" class="uppercase" required>'.PHP_EOL;
echo '						<option value="">----</option>'.PHP_EOL;
for($i = 0;$i < sizeof($subject);$i++){
echo '						<option value="'.$subject[$i]['codtipodomanda'].'" class="uppercase">'.$subject[$i]['nome'].'</option>'.PHP_EOL;
}
echo '					</select>	'.PHP_EOL;
echo '				</div>'.PHP_EOL;
echo '			</div>'.PHP_EOL;
//echo '			<div class="col-4-12">'.PHP_EOL;
//echo '				  <div class="formElement minTopMarg">'.PHP_EOL;
//echo '					  <input value="Ricerca" class="great uppercase" data-button="normal" type="submit" name="submit">'.PHP_EOL;
//echo '				  </div>'.PHP_EOL;			
//echo '		  </div>'.PHP_EOL;
echo '    </div>'.PHP_EOL;
echo '    <hr>'.PHP_EOL;
echo '</form>'.PHP_EOL;
echo '<div class="screenFlex flexColumn">'.PHP_EOL;
echo '  <h2 class="center uppercase">Lista domande</h2>'.PHP_EOL;
echo '  <hr>'.PHP_EOL;
echo '		<div class="flexRow flex flexBetween marginTopButton">'.PHP_EOL;
echo '			<div class="col-3-12 center">'.PHP_EOL;
echo '        <span class="uppercase bold">Codice domanda:</span>'.PHP_EOL;
echo '      </div>'.PHP_EOL;
echo '			<div class="col-3-12 center">'.PHP_EOL;
echo '        <span class="uppercase bold">Tipo di domanda:</span>'.PHP_EOL;
echo '      </div>'.PHP_EOL;
echo '			<div class="col-3-12 center">'.PHP_EOL;
echo '        <span class="uppercase bold">Domanda:</span>'.PHP_EOL;
echo '      </div>'.PHP_EOL;
echo '			<div class="col-3-12 center">'.PHP_EOL;
echo '        <span class="uppercase bold">Mostra di più:</span>'.PHP_EOL;
echo '      </div>'.PHP_EOL;
echo '    </div>'.PHP_EOL;
echo '  <hr>'.PHP_EOL;
echo '</div>'.PHP_EOL;
echo '<div class="screenFlex flexColumn overFlowYscroll heightResp" data-load>'.PHP_EOL;
/*for($i=0;$i<sizeOf($result); $i++){
echo '  <div class="flexRow flex flexBetween marginTopButton row">'.PHP_EOL;
echo '	  <div class="col-3-12 center">'.PHP_EOL;
echo '      <span class="uppercase bold">'. $result[$i]['coddomanda'] .'</span>'.PHP_EOL;
echo '    </div>'.PHP_EOL;
echo '	  <div class="col-3-12 center">'.PHP_EOL;
$stampSubject = $result[$i]['codtipodomanda'];
echo '      <span class="uppercase bold">'. $subject[$stampSubject]['nome'] .'</span>'.PHP_EOL;
echo '    </div>'.PHP_EOL;
echo '	  <div class="col-3-12 center">'.PHP_EOL;
echo '      <span class="uppercase bold">'. $result[$i]['domanda'] .'</span>'.PHP_EOL;
echo '    </div>'.PHP_EOL;
echo '	  <div class="col-3-12 center">'.PHP_EOL;
echo '      <span class="uppercase bold">Lorem Ipsum</span>'.PHP_EOL;
echo '    </div>'.PHP_EOL;
echo '  </div>'.PHP_EOL;
echo '    <hr>'.PHP_EOL;
}*/
echo '</div>'.PHP_EOL;
echo '<div class="screenFlex minMargBottom">'.PHP_EOL;
echo '		<div class="col-12-12 center">'.PHP_EOL;
echo '      <hr>'.PHP_EOL;
echo '    </div>'.PHP_EOL;
echo '</div>'.PHP_EOL;


//								|
//								|
//								|
//								V

/*-------------------------FINESTRE-MODALI---------------------*/

//------------> FINESTRA-MODALE-DI-ERRORE

echo '<div class="maxModalScreen" id="modalScreenError">'.PHP_EOL;
echo '	<div class="modal darklightgrey centerBox">'.PHP_EOL;
echo '		<div class="closeIcon">'.PHP_EOL;
echo '			<img src="../img/png/iconClose.png" width="64" height="64" alt="icon Close Modal" class="modalImage">'.PHP_EOL;
echo '		</div>'.PHP_EOL;
echo '		<div class="textModal center">'.PHP_EOL;
echo '			<p class="red bold">Errore generico che puo essere derivato dai seguenti fattori:</p>'.PHP_EOL;
echo '			<ol>'.PHP_EOL;
echo '				<li>Errore generico server, se il problema sussiste contattare l\'amministratore</li>'.PHP_EOL;
echo '			</ol>'.PHP_EOL;
echo '		</div>'.PHP_EOL;
echo '		<div class="screenBtn">'.PHP_EOL;
echo '			<div class="col-12-12">'.PHP_EOL;
echo '				<input value="Chiudi" class="uppercase medium no-round buttonSelected  no-border" data-button="danger" type="button">'.PHP_EOL;
echo '			</div>'.PHP_EOL;	
echo '		</div>'.PHP_EOL;
echo '	</div>'.PHP_EOL;
echo '</div>'.PHP_EOL;
echo '<script src="../js/searchQuery.js" defer></script>'.PHP_EOL;
?>