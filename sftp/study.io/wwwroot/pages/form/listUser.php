<?php
/*-------------------------------INCLUSION-FILE-----------------------------*/

	include $_SERVER['DOCUMENT_ROOT'] . '/pages/config/config_db.php';
	include $_SERVER['DOCUMENT_ROOT'] . '/pages/function/my_function.php';

/*----------------------------------------------------------------------------*/
	
	$database = new study_io;
	$conn = $database->connect_db();
	$result = $database->printUsers($conn);
	// print_r($result);

/*-------------------------------------------------------*/

echo '	<div class="screen screenFlex flexColumn">'.PHP_EOL;
echo '		<div class="row">'.PHP_EOL;
echo '			<div class="col-12-12">'.PHP_EOL;
echo '				<h1 class="center uppercase">Rimuovi / modifica utente</h1>'.PHP_EOL;
echo '			</div>'.PHP_EOL;
echo '		</div>'.PHP_EOL;
echo '		<div class="flexRow flex flexBetween marginTopButton hiddenResp">'.PHP_EOL;
echo '			<div class="col-4-12 center">'.PHP_EOL;
echo '             <span class="uppercase bold">Nome</span>'.PHP_EOL;
echo '			</div>'.PHP_EOL;
echo '			<div class="col-4-12 center">'.PHP_EOL;
echo '             <span class="uppercase bold">Cognome</span>'.PHP_EOL;
echo '			</div>'.PHP_EOL;
echo '			<div class="col-4-12 center">'.PHP_EOL;
echo '             <span class="uppercase bold">e-mail</span>'.PHP_EOL;
echo '			</div>'.PHP_EOL;
echo '		</div>';
for ($i = 0; $i < sizeof($result); $i++) {
	echo '		<hr>'.PHP_EOL;
	echo '		<div class="flexRow flex flexBetween marginTopButton">'.PHP_EOL;
	echo '			<div class="col-4-12 center minPaddingLeftRightTop">'.PHP_EOL;
	echo '             <span class="uppercase bold">'. $result[$i]['nome'] .'</span>'.PHP_EOL;
	echo '			</div>'.PHP_EOL;
	echo '			<div class="col-4-12 center minPaddingLeftRightTop">'.PHP_EOL;
	echo '             <span class="uppercase bold">'. $result[$i]['cognome'] .'</span>'.PHP_EOL;
	echo '			</div>'.PHP_EOL;
		echo '			<div class="col-4-12 center minPaddingLeftRightTop">'.PHP_EOL;
	echo '             <span class="uppercase bold">'. $result[$i]['email'] .'</span>'.PHP_EOL;
	echo '			</div>'.PHP_EOL;
	echo '	</div>'.PHP_EOL;
}
echo '		<hr>'.PHP_EOL;
echo '	</div>'.PHP_EOL;

?>