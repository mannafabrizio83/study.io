<?php
/*-------------------------------INCLUSION-FILE-----------------------------*/

	include $_SERVER['DOCUMENT_ROOT'] . '/pages/config/config_db.php';
	include $_SERVER['DOCUMENT_ROOT'] . '/pages/function/my_function.php';

/*----------------------------------------------------------------------------*/

	$database = new study_io;
	$conn = $database->connect_db();
	$result = $database->printSettings($conn);

/*-------------------------------------------------------*/

echo '<form action="functionPages/functionSettingTest.php" method="post" autocomplete="off" name="settingsForm">'.PHP_EOL;
echo '	<div class="screen screenFlex flexColumn">'.PHP_EOL;
echo '		<div class="row">'.PHP_EOL;
echo '			<div class="col-12-12">'.PHP_EOL;
echo '				<h1 class="center uppercase">Settaggi domande</h1>'.PHP_EOL;
echo '			</div>'.PHP_EOL;
echo '		</div>'.PHP_EOL;
echo '		<hr>'.PHP_EOL;
echo '		<div class="flexRow flex flexBetween marginTopButton">'.PHP_EOL;
echo '			<div class="col-4-12 formElement">'.PHP_EOL;
echo '				<label for="nPhysics" class="uppercase">Numero di domande di fisica:</label>'.PHP_EOL;
echo '				<select name="physics" id="nPhysics">'.PHP_EOL;
echo '					<option value="">----</option>'.PHP_EOL;
					for($i = 1 ; $i <= 100; $i++){
						if ( $i == $result['fisica']){
echo '					<option value="'.$i.'" selected>'.$i.'</option>'.PHP_EOL;
						}
						else{
echo '					<option value="'.$i.'">'.$i.'</option>'.PHP_EOL;
						}
					}
echo '				</select>	'.PHP_EOL;
echo '			</div>'.PHP_EOL;
echo '			<div class="col-4-12 formElement">'.PHP_EOL;
echo '				<label for="nMaths" class="uppercase">Numero di domande di matematica:</label>'.PHP_EOL;
echo '				<select name="maths" id="nMaths">'.PHP_EOL;
echo '					<option value="">----</option>'.PHP_EOL;
					for($i = 1 ; $i <= 100; $i++){
						if ( $i == $result['matematica']){
echo '					<option value="'.$i.'" selected>'.$i.'</option>'.PHP_EOL;
						}
						else{
echo '					<option value="'.$i.'">'.$i.'</option>'.PHP_EOL;
						}
					}
echo '				</select>	'.PHP_EOL;
echo '			</div>'.PHP_EOL;
echo '		</div>'.PHP_EOL;
echo '		<hr>'.PHP_EOL;
echo '		<div class="flexRow flex flexBetween marginTopButton">'.PHP_EOL;
echo '			<div class="col-4-12 formElement">'.PHP_EOL;
echo '				<label for="nChemistry" class="uppercase">Numero di domande di chimica:</label>'.PHP_EOL;
echo '				<select name="chemistry" id="nChemistry">'.PHP_EOL;
echo '					<option value="">----</option>'.PHP_EOL;
					for($i = 1 ; $i <= 100; $i++){
						if ( $i == $result['chimica']){
echo '					<option value="'.$i.'" selected>'.$i.'</option>'.PHP_EOL;
						}
						else{
echo '					<option value="'.$i.'">'.$i.'</option>'.PHP_EOL;
						}
					}
echo '				</select>	'.PHP_EOL;
echo '			</div>'.PHP_EOL;
echo '			<div class="col-4-12 formElement">'.PHP_EOL;
echo '				<label for="nBiology" class="uppercase">Numero di domande di biologia:</label>'.PHP_EOL;
echo '				<select name="biology" id="nBiology">'.PHP_EOL;
echo '					<option value="">----</option>'.PHP_EOL;
					for($i = 1 ; $i <= 100; $i++){
						if ( $i == $result['biologia']){
echo '					<option value="'.$i.'" selected>'.$i.'</option>'.PHP_EOL;
						}
						else{
echo '					<option value="'.$i.'">'.$i.'</option>'.PHP_EOL;
						}
					}
echo '				</select>	'.PHP_EOL;
echo '			</div>'.PHP_EOL;
echo '		</div>'.PHP_EOL;
echo '		<hr>'.PHP_EOL;
echo '		<div class="flexRow flex flexBetween marginTopButton">'.PHP_EOL;
echo '			<div class="col-4-12 formElement">'.PHP_EOL;
echo '				<label for="nLogic" class="uppercase">Numero di domande di logica:</label>'.PHP_EOL;
echo '				<select name="logic" id="nLogic">'.PHP_EOL;
echo '					<option value="">----</option>'.PHP_EOL;
					for($i = 1 ; $i <= 100; $i++){
						if ( $i == $result['logica']){
echo '					<option value="'.$i.'" selected>'.$i.'</option>'.PHP_EOL;
						}
						else{
echo '					<option value="'.$i.'">'.$i.'</option>'.PHP_EOL;
						}
					}
echo '				</select>	'.PHP_EOL;
echo '			</div>'.PHP_EOL;
echo '			<div class="col-4-12 formElement">'.PHP_EOL;
echo '				<label for="nGeneralCulture" class="uppercase">Numero di domande di cultura generale:</label>'.PHP_EOL;
echo '				<select name="generalCulture" id="nGeneralCulture">'.PHP_EOL;
echo '					<option value="">----</option>'.PHP_EOL;
					for($i = 1 ; $i <= 100; $i++){
						if ( $i == $result['cultura_generale']){
echo '					<option value="'.$i.'" selected>'.$i.'</option>'.PHP_EOL;
						}
						else{
echo '					<option value="'.$i.'">'.$i.'</option>'.PHP_EOL;
						}
					}
echo '				</select>	'.PHP_EOL;
echo '			</div>'.PHP_EOL;
echo '		</div>'.PHP_EOL;
echo '		<hr>'.PHP_EOL;
echo '		<div class="flexRow flex flexBetween marginTopButton">'.PHP_EOL;
echo '			<div class="col-4-12 formElement">'.PHP_EOL;
echo '				<label for="nRespForAllQuery" class="uppercase">Numero di risposte per ogni domanda:</label>'.PHP_EOL;
echo '				<select name="nRespForAllQuery" id="nRespForAllQuery">'.PHP_EOL;
echo '					<option value="">----</option>'.PHP_EOL;
					for($i = 1 ; $i <= 100; $i++){
						if ( $i == $result['tot_risposte']){
echo '					<option value="'.$i.'" selected>'.$i.'</option>'.PHP_EOL;
						}
						else{
echo '					<option value="'.$i.'">'.$i.'</option>'.PHP_EOL;
						}
					}
echo '				</select>	'.PHP_EOL;
echo '			</div>'.PHP_EOL;
echo '		</div>'.PHP_EOL;
echo '		<hr>'.PHP_EOL;
echo '		<div class="flexRow flex marginTopButton toRightFlex">'.PHP_EOL;
echo '			<div class="col-4-12 formElement">'.PHP_EOL;
echo '				<div class="formElement minTopMarg">'.PHP_EOL;
echo '					<input value="Aggiorna settaggi" class="great uppercase" data-button="normal" type="submit" name="submit">'.PHP_EOL;
echo '				</div>'.PHP_EOL;
echo '			</div>'.PHP_EOL;
echo '		</div>'.PHP_EOL;
echo '	</div>'.PHP_EOL;
echo '	<script src="../js/settingsTest.js" defer></script>'.PHP_EOL;
echo '</form>'.PHP_EOL;


//								|
//								|
//								|
//								V

/*-------------------------FINESTRE-MODALI---------------------*/

//------------> FINESTRA-MODALE-DI-ERRORE

echo '<div class="maxModalScreen" id="modalScreenError">'.PHP_EOL;
echo '	<div class="modal darklightgrey centerBox">'.PHP_EOL;
echo '		<div class="closeIcon">'.PHP_EOL;
echo '			<img src="../img/png/iconClose.png" width="64" height="64" alt="icon Close Modal" class="modalImage">'.PHP_EOL;
echo '		</div>'.PHP_EOL;
echo '		<div class="textModal center">'.PHP_EOL;
echo '			<p class="red bold">Errore generico che puo essere derivato dai seguenti fattori:</p>'.PHP_EOL;
echo '			<ol>'.PHP_EOL;
echo '				<li>Dati inseriti in modo scorretto</li>'.PHP_EOL;
echo '				<li>Compila tutti i campi</li>'.PHP_EOL;
echo '				<li>Errore generico server contattare l\'amministratore</li>'.PHP_EOL;
echo '			</ol>'.PHP_EOL;
echo '		</div>'.PHP_EOL;
echo '		<div class="screenBtn">'.PHP_EOL;
echo '			<div class="col-12-12">'.PHP_EOL;
echo '				<input value="Chiudi" class="uppercase medium no-round buttonSelected  no-border" data-button="danger" type="button">'.PHP_EOL;
echo '			</div>'.PHP_EOL;	
echo '		</div>'.PHP_EOL;
echo '	</div>'.PHP_EOL;
echo '</div>'.PHP_EOL;

//------------> FINESTRA-MODALE-DI-OPERAZIONE-ESEGUITA

echo '<div class="maxModalScreen" id="modalScreenAccept">'.PHP_EOL;
echo '	<div class="modal darklightgrey centerBox">'.PHP_EOL;
echo '		<div class="closeIcon">'.PHP_EOL;
echo '			<img src="../img/png/iconClose.png" width="64" height="64" alt="icon Close Modal" class="modalImage">'.PHP_EOL;
echo '		</div>'.PHP_EOL;
echo '		<div class="textModal center">'.PHP_EOL;
echo '			<p class="capitalize">Dati aggiornati correttamente premi ok per proseguire</p>'.PHP_EOL;
echo '		</div>'.PHP_EOL;
echo '		<div class="screenBtn">'.PHP_EOL;
echo '			<div class="col-12-12">'.PHP_EOL;
echo '				<input value="OK" class="uppercase medium no-round buttonSelected no-border" data-button="accept" type="button">'.PHP_EOL;
echo '			</div>'.PHP_EOL;	
echo '		</div>'.PHP_EOL;
echo '	</div>'.PHP_EOL;
echo '</div>'.PHP_EOL;	
?>