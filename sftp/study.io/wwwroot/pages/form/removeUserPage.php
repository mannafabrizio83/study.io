<?php
/*-------------------------------INCLUSION-FILE-----------------------------*/

	include $_SERVER['DOCUMENT_ROOT'] . '/pages/config/config_db.php';
	include $_SERVER['DOCUMENT_ROOT'] . '/pages/function/my_function.php';

/*----------------------------------------------------------------------------*/
	
	$database = new study_io;
	$conn = $database->connect_db();
	$result = $database->printUsers($conn);
	// print_r($result);

/*-------------------------------------------------------*/

echo '	<div class="screen screenFlex flexColumn">'.PHP_EOL;
echo '		<div class="row">'.PHP_EOL;
echo '			<div class="col-12-12">'.PHP_EOL;
echo '				<h1 class="center uppercase">Rimuovi / modifica utente</h1>'.PHP_EOL;
echo '			</div>'.PHP_EOL;
echo '		</div>'.PHP_EOL;
echo '		<div class="flexRow flex flexBetween marginTopButton hiddenResp">'.PHP_EOL;
echo '			<div class="col-3-12 center">'.PHP_EOL;
echo '             <span class="uppercase bold">Nome</span>'.PHP_EOL;
echo '			</div>'.PHP_EOL;
echo '			<div class="col-3-12 center">'.PHP_EOL;
echo '             <span class="uppercase bold">Cognome</span>'.PHP_EOL;
echo '			</div>'.PHP_EOL;
echo '			<div class="col-3-12 center">'.PHP_EOL;
echo '             <span class="uppercase bold">Modifica</span>'.PHP_EOL;
echo '			</div>'.PHP_EOL;
echo '			<div class="col-3-12 center">'.PHP_EOL;
echo '             <span class="uppercase bold">Rimuovi</span>'.PHP_EOL;
echo '			</div>'.PHP_EOL;
echo '		</div>';
for ($i = 0; $i < sizeof($result); $i++) {
	echo '		<hr>'.PHP_EOL;
	echo '		<div class="flexRow flex flexBetween marginTopButton">'.PHP_EOL;
	echo '			<div class="col-3-12 center minPaddingLeftRightTop">'.PHP_EOL;
	echo '             <span class="uppercase bold">'. $result[$i]['nome'] .'</span>'.PHP_EOL;
	echo '			</div>'.PHP_EOL;
	echo '			<div class="col-3-12 center minPaddingLeftRightTop">'.PHP_EOL;
	echo '             <span class="uppercase bold">'. $result[$i]['cognome'] .'</span>'.PHP_EOL;
	echo '			</div>'.PHP_EOL;
	echo '			<div class="col-3-12 minPaddingLeftRightTop center">'.PHP_EOL;
	echo '             <span class="uppercase bold"><input value="Modifica" class="great" data-button="warning" type="button" data-user="uid='.$result[$i]['coduser'].'&'.'email='.$result[$i]['email'].'&'.'sendProcess=1'.'"></span>'.PHP_EOL;
	echo '			</div>'.PHP_EOL;
	echo '			<div class="col-3-12 minPaddingLeftRightTop center">'.PHP_EOL;
	echo '             <span class="uppercase bold"><input value="Rimuovi" class="great" data-button="danger" type="button" data-user="uid='.$result[$i]['coduser'].'&'.'email='.$result[$i]['email'].'&'.'sendProcess=2'.'"></span>'.PHP_EOL;
	echo '			</div>'.PHP_EOL;
	echo '		</div>';
}
echo '		<hr>'.PHP_EOL;
echo '	</div>'.PHP_EOL;

//								|
//								|
//								|
//								V

/*-------------------------FINESTRE-MODALI---------------------*/

//------------> FINESTRA-MODALE-DI-ERRORE

echo '<div class="maxModalScreen" id="modalScreenError">'.PHP_EOL;
echo '	<div class="modal darklightgrey centerBox">'.PHP_EOL;
echo '		<div class="closeIcon">'.PHP_EOL;
echo '			<img src="../img/png/iconClose.png" width="64" height="64" alt="icon Close Modal" class="modalImage">'.PHP_EOL;
echo '		</div>'.PHP_EOL;
echo '		<div class="textModal center">'.PHP_EOL;
echo '			<p class="red bold">Errore generico che puo essere derivato dai seguenti fattori:</p>'.PHP_EOL;
echo '			<ol>'.PHP_EOL;
echo '				<li>Errore generico server contattare l\'amministratore</li>'.PHP_EOL;
echo '			</ol>'.PHP_EOL;
echo '		</div>'.PHP_EOL;
echo '		<div class="screenBtn">'.PHP_EOL;
echo '			<div class="col-12-12">'.PHP_EOL;
echo '				<input value="Chiudi" class="uppercase medium no-round buttonSelected  no-border" data-button="danger" type="button">'.PHP_EOL;
echo '			</div>'.PHP_EOL;	
echo '		</div>'.PHP_EOL;
echo '	</div>'.PHP_EOL;
echo '</div>'.PHP_EOL;

//------------> FINESTRA-MODALE-DI-OPERAZIONE-ESEGUITA

echo '<div class="maxModalScreen" id="modalScreenAccept">'.PHP_EOL;
echo '	<div class="modal darklightgrey centerBox">'.PHP_EOL;
echo '		<div class="closeIcon">'.PHP_EOL;
echo '			<img src="../img/png/iconClose.png" width="64" height="64" alt="icon Close Modal" class="modalImage">'.PHP_EOL;
echo '		</div>'.PHP_EOL;
echo '		<div class="textModal center">'.PHP_EOL;
echo '			<p class="capitalize">Utente modificato / rimosso in modo corretto</p>'.PHP_EOL;
echo '		</div>'.PHP_EOL;
echo '		<div class="screenBtn">'.PHP_EOL;
echo '			<div class="col-12-12">'.PHP_EOL;
echo '				<input value="OK" class="uppercase medium no-round buttonSelected no-border" data-button="accept" type="button">'.PHP_EOL;
echo '			</div>'.PHP_EOL;	
echo '		</div>'.PHP_EOL;
echo '	</div>'.PHP_EOL;
echo '</div>'.PHP_EOL;	
echo '<script src="../js/jsRemUser.js"></script>'.PHP_EOL;
?>