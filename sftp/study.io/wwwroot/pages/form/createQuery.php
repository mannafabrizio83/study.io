<?php
/*-------------------------------INCLUSION-FILE-----------------------------*/

	include $_SERVER['DOCUMENT_ROOT'] . '/pages/config/config_db.php';
	include $_SERVER['DOCUMENT_ROOT'] . '/pages/function/my_function.php';

/*----------------------------------------------------------------------------*/
	
	$database = new study_io;
	$conn = $database->connect_db();
	$result = $database->createQuery($conn);
	// print_r($result);
	$resultSecondQuery = $database->printSubject($conn);
	//var_dump($resultSecondQuery);

/*-------------------------------------------------------*/

echo '<form action="functionPages/functionCreateQuery.php" method="post" autocomplete="off" name="createUserForm" enctype="multipart/form-data">'.PHP_EOL;
echo '	<div class="screen screenFlex flexColumn">'.PHP_EOL;
echo '		<div class="row">'.PHP_EOL;
echo '			<div class="col-12-12">'.PHP_EOL;
echo '				<h1 class="center uppercase">Crea domanda </h1>'.PHP_EOL;
echo '			</div>'.PHP_EOL;
echo '		</div>'.PHP_EOL;
echo '		<hr>'.PHP_EOL;
echo '		<div class="flexRow flex flexBetween marginTopButton">'.PHP_EOL;
echo '			<div class="col-12-12">'.PHP_EOL;
echo '					<label for="subjects" class="uppercase">Scegli materia:</label>'.PHP_EOL;
echo '					<select name="subjects" id="subjects" class="uppercase">'.PHP_EOL;
echo '						<option value="" required>----</option>'.PHP_EOL;
for($i=0; $i < sizeof($resultSecondQuery); $i++){
echo '						<option value="'.$resultSecondQuery[$i]['codtipodomanda'].'" class="uppercase">'.$resultSecondQuery[$i]['nome'].'</option>'.PHP_EOL;
}
echo '					</select>'.PHP_EOL;
echo '			</div>'.PHP_EOL;
echo '		</div>'.PHP_EOL;
echo '		<hr>'.PHP_EOL;
echo '		<div class="flexRow flex flexBetween marginTopButton">'.PHP_EOL;
echo '			<div class="col-12-12 ovHide relatives">'.PHP_EOL;
echo '				<div class="formElement minTopMarg">'.PHP_EOL;
echo '					<label for="file" class="uppercase">Inserisci immagine: ( Non obbligatorio )</label>'.PHP_EOL;
echo '					<input type="file" name="files" multiple id="file">'.PHP_EOL;
echo '				</div>'.PHP_EOL;
echo '			</div>'.PHP_EOL;
echo '		</div>'.PHP_EOL;
echo '		<hr>'.PHP_EOL;
echo '		<div class="flexRow flex flexBetween marginTopButton">'.PHP_EOL;
echo '			<div class="col-12-12">'.PHP_EOL;
echo '				<div class="formElement minTopMarg">'.PHP_EOL;
echo '					<label class="uppercase" for="queyUser">Domanda da sottoporre:</label>'.PHP_EOL;
echo '					<textarea required id="queryUser" name="queryUser" class="minBox" required></textarea>'.PHP_EOL;
echo '				</div>'.PHP_EOL;
echo '			</div>'.PHP_EOL;
echo '		</div>'.PHP_EOL;
echo '		<hr>'.PHP_EOL;
echo '		<div class="flexRow flex flexBetween marginTopButton">'.PHP_EOL;
echo '			<div class="col-12-12">'.PHP_EOL;
echo '				<div class="formElement minTopMarg">'.PHP_EOL;
echo '					<label class="uppercase" for="firstRespCorrect">Risposta corretta:</label>'.PHP_EOL;
echo '					<textarea required id="firstRespCorrect" name="firstRespCorrect" class="minBox" required></textarea>'.PHP_EOL;
echo '				</div>'.PHP_EOL;
echo '			</div>'.PHP_EOL;
echo '		</div>'.PHP_EOL;
for($i = 1; $i <= ($result[0]['tot_risposte'] - 1); $i++){
	echo ' 	<hr>'.PHP_EOL;
	echo '	<div class="flexRow flex flexBetween marginTopButton">'.PHP_EOL;
	echo '			<div class="col-12-12">'.PHP_EOL;
	echo '				<div class="formElement minTopMarg">'.PHP_EOL;
	echo '					<label class="uppercase" for="firstResp['.$i.']">Risposta Errata:</label>'.PHP_EOL;
	echo '					<textarea required id="firstResp['.$i.']" name="firstResp['.$i.']" class="minBox" required></textarea>'.PHP_EOL;
	echo '				</div>'.PHP_EOL;
	echo '		</div>'.PHP_EOL;
	echo '	</div>'.PHP_EOL;
}
echo '		<hr>'.PHP_EOL;
echo '		<div class="flexRow flex marginTopButton toRightFlex">'.PHP_EOL;
echo '			<div class="col-4-12">'.PHP_EOL;
echo '				<div class="formElement minTopMarg">'.PHP_EOL;
echo '					<input value="Invia dati" class="great uppercase" data-button="normal" type="submit" name="submit">'.PHP_EOL;
echo '				</div>'.PHP_EOL;			
echo '			</div>'.PHP_EOL;
echo '		</div>'.PHP_EOL;
echo '	</div>'.PHP_EOL;
echo '</form>'.PHP_EOL;
echo '<script src="../js/createQuery.js"></script>'.PHP_EOL;


//								|
//								|
//								|
//								V

/*-------------------------FINESTRE-MODALI---------------------*/

//------------> FINESTRA-MODALE-DI-ERRORE

echo '<div class="maxModalScreen" id="modalScreenError">'.PHP_EOL;
echo '	<div class="modal darklightgrey centerBox">'.PHP_EOL;
echo '		<div class="closeIcon">'.PHP_EOL;
echo '			<img src="../img/png/iconClose.png" width="64" height="64" alt="icon Close Modal" class="modalImage">'.PHP_EOL;
echo '		</div>'.PHP_EOL;
echo '		<div class="textModal center">'.PHP_EOL;
echo '			<p class="red bold">Errore generico che puo essere derivato dai seguenti fattori:</p>'.PHP_EOL;
echo '			<ol>'.PHP_EOL;
echo '				<li>Errore generico server, se il problema sussiste contattare l\'amministratore</li>'.PHP_EOL;
echo '			</ol>'.PHP_EOL;
echo '		</div>'.PHP_EOL;
echo '		<div class="screenBtn">'.PHP_EOL;
echo '			<div class="col-12-12">'.PHP_EOL;
echo '				<input value="Chiudi" class="uppercase medium no-round buttonSelected  no-border" data-button="danger" type="button">'.PHP_EOL;
echo '			</div>'.PHP_EOL;	
echo '		</div>'.PHP_EOL;
echo '	</div>'.PHP_EOL;
echo '</div>'.PHP_EOL;

//------------> FINESTRA-MODALE-DI-OPERAZIONE-ESEGUITA

echo '<div class="maxModalScreen" id="modalScreenAccept">'.PHP_EOL;
echo '	<div class="modal darklightgrey centerBox">'.PHP_EOL;
echo '		<div class="closeIcon">'.PHP_EOL;
echo '			<img src="../img/png/iconClose.png" width="64" height="64" alt="icon Close Modal" class="modalImage">'.PHP_EOL;
echo '		</div>'.PHP_EOL;
echo '		<div class="textModal center">'.PHP_EOL;
echo '			<p class="capitalize">Dati caricati correttamente clicca ok per proseguire</p>'.PHP_EOL;
echo '		</div>'.PHP_EOL;
echo '		<div class="screenBtn">'.PHP_EOL;
echo '			<div class="col-12-12">'.PHP_EOL;
echo '				<input value="OK" class="uppercase medium no-round buttonSelected no-border" data-button="accept" type="button">'.PHP_EOL;
echo '			</div>'.PHP_EOL;	
echo '		</div>'.PHP_EOL;
echo '	</div>'.PHP_EOL;
echo '</div>'.PHP_EOL;	

?>