<?php
echo '<form action="functionPages/functionCreateUser.php" method="post" autocomplete="off" name="createUserForm">'.PHP_EOL;
echo '	<div class="screen screenFlex flexColumn">'.PHP_EOL;
echo '		<div class="row">'.PHP_EOL;
echo '			<div class="col-12-12">'.PHP_EOL;
echo '				<h1 class="center uppercase">Crea utente </h1>'.PHP_EOL;
echo '			</div>'.PHP_EOL;
echo '		</div>'.PHP_EOL;
echo '		<hr>'.PHP_EOL;
echo '		<div class="flexRow flex flexBetween marginTopButton">'.PHP_EOL;
echo '			<div class="col-5-12">'.PHP_EOL;
echo '				<div class="formElement minTopMarg">'.PHP_EOL;
echo '					<label class="uppercase" for="name">Nome:</label>'.PHP_EOL;
echo '					<input type="text" id="name"  name="nameUser" required placeholder="Inserisci Nome">'.PHP_EOL;
echo '				</div>'.PHP_EOL;
echo '			</div>'.PHP_EOL;
echo '			<div class="col-5-12">'.PHP_EOL;
echo '				<div class="formElement minTopMarg">'.PHP_EOL;
echo '					<label class="uppercase" for="surname">Cognome:</label>'.PHP_EOL;
echo '					<input type="text" id="surname"  name="surnameUser" required placeholder="Inserisci Cognome">'.PHP_EOL;
echo '				</div>'.PHP_EOL;
echo '			</div>'.PHP_EOL;
echo '		</div>'.PHP_EOL;
echo '		<hr>'.PHP_EOL;
echo '		<div class="flexRow flex flexBetween marginTopButton">'.PHP_EOL;
echo '			<div class="col-5-12">'.PHP_EOL;
echo '				<div class="formElement minTopMarg">'.PHP_EOL;
echo '					<label class="uppercase" for="eMail">E-mail:</label>'.PHP_EOL;
echo '					<input type="email" id="eMail"  name="eMail" data-form="email" required placeholder="Inserisci e-mail">'.PHP_EOL;
echo '				</div>			'.PHP_EOL;
echo '			</div>'.PHP_EOL;
echo '			<div class="col-5-12">'.PHP_EOL;
echo '				<div class="formElement minTopMarg">'.PHP_EOL;
echo '					<label class="uppercase" for="tel">Telefono/Cell:</label>'.PHP_EOL;
echo '					<input type="tel" maxlength="11" id="tel"  name="telUser" required placeholder="Inserisci numero">'.PHP_EOL;
echo '				</div>			'.PHP_EOL;
echo '			</div>'.PHP_EOL;
echo '		</div>'.PHP_EOL;
echo '		<hr>'.PHP_EOL;
echo '		<div class="flexRow flex flexBetween marginTopButton">'.PHP_EOL;
echo '			<div class="col-4-12">'.PHP_EOL;
echo '				<div class="formElement minTopMarg">'.PHP_EOL;
echo '					<label class="uppercase" for="admin">Admin ?</label>'.PHP_EOL;
echo '					<label class="checkboxDescktop">'.PHP_EOL;
echo '	  					<input type="checkbox" data-toogle="checkAdmin" class="checked" name="admin">'.PHP_EOL;
echo '	  					<span class="check"></span>'.PHP_EOL;
echo '					</label>'.PHP_EOL;
echo '				</div>		'.PHP_EOL;
echo '			</div>'.PHP_EOL;
//echo '	    <div class="col-4-12">'.PHP_EOL;
//echo '				<div class="formElement minTopMarg" id="checkUniversity">'.PHP_EOL;
//echo '					<label class="uppercase" for="admin">Seleziona Università</label>'.PHP_EOL;
//echo '					<label class="checkboxDescktop">'.PHP_EOL;
//echo '	  					<input type="checkbox" data-toogle="checkuniversity" class="checked" name="admin">'.PHP_EOL;
//echo '	  					<span class="check"></span>'.PHP_EOL;
//echo '					</label>'.PHP_EOL;
//echo '				</div>		'.PHP_EOL;
//echo '			</div>'.PHP_EOL;
//echo '			<div class="col-4-12" id="uniSelect">'.PHP_EOL;
//echo '				<div class="formElement minTopMarg" id="textUniversity">'.PHP_EOL;
//echo '					<label class="uppercase" for="textUni">Scrivi corso universitario:</label>'.PHP_EOL;
//echo '					<input type="text" id="textUni"  name="textUni" placeholder="Inserisci corso universitario">'.PHP_EOL;
//echo '				</div>			'.PHP_EOL;
//echo '				<div class="formElement minTopMarg" id="selectUniversity">'.PHP_EOL;
//echo '					<label for="university" class="uppercase">Scegli Università:</label>'.PHP_EOL;
//echo '					<select name="university" id="university">'.PHP_EOL;
//echo '						<option value="">----</option>'.PHP_EOL;
//echo '						<option value="1">1</option>'.PHP_EOL;
//echo '						<option value="2">2</option>'.PHP_EOL;
//echo '						<option value="3">3</option>'.PHP_EOL;
//echo '						<option value="4">4</option>'.PHP_EOL;
//echo '					</select>	'.PHP_EOL;
//echo '				</div>'.PHP_EOL;
//echo '			</div>'.PHP_EOL;
echo '		</div>'.PHP_EOL;
echo '		<hr>'.PHP_EOL;
echo '		<div class="flexRow flex marginTopButton toRightFlex">'.PHP_EOL;
echo '			<div class="col-4-12">'.PHP_EOL;
echo '				<div class="formElement minTopMarg">'.PHP_EOL;
echo '					<input value="Invia dati" class="great uppercase" data-button="normal" type="submit">'.PHP_EOL;
echo '				</div>			'.PHP_EOL;
echo '			</div>'.PHP_EOL;
echo '		</div>'.PHP_EOL;
echo '		<script src="../js/jsCreateUser.js" defer></script>'.PHP_EOL;
echo '	</div>'.PHP_EOL;
echo '</form>'.PHP_EOL;

//								|
//								|
//								|
//								V

/*-------------------------FINESTRE-MODALI---------------------*/

//------------> FINESTRA-MODALE-DI-ERRORE

echo '<div class="maxModalScreen" id="modalScreenError">'.PHP_EOL;
echo '	<div class="modal darklightgrey centerBox">'.PHP_EOL;
echo '		<div class="closeIcon">'.PHP_EOL;
echo '			<img src="../img/png/iconClose.png" width="64" height="64" alt="icon Close Modal" class="modalImage">'.PHP_EOL;
echo '		</div>'.PHP_EOL;
echo '		<div class="textModal center">'.PHP_EOL;
echo '			<p class="red bold">Errore generico che puo essere derivato dai seguenti fattori:</p>'.PHP_EOL;
echo '			<ol>'.PHP_EOL;
echo '				<li>Dati inseriti in modo scorretto</li>'.PHP_EOL;
echo '				<li>Compila tutti i campi</li>'.PHP_EOL;
echo '				<li>Errore generico server contattare l\'amministratore</li>'.PHP_EOL;
echo '			</ol>'.PHP_EOL;
echo '		</div>'.PHP_EOL;
echo '		<div class="screenBtn">'.PHP_EOL;
echo '			<div class="col-12-12">'.PHP_EOL;
echo '				<input value="Chiudi" class="uppercase medium no-round buttonSelected  no-border" data-button="danger" type="button">'.PHP_EOL;
echo '			</div>'.PHP_EOL;	
echo '		</div>'.PHP_EOL;
echo '	</div>'.PHP_EOL;
echo '</div>'.PHP_EOL;

//------------> FINESTRA-MODALE-DI-OPERAZIONE-ESEGUITA

echo '<div class="maxModalScreen" id="modalScreenAccept">'.PHP_EOL;
echo '	<div class="modal darklightgrey centerBox">'.PHP_EOL;
echo '		<div class="closeIcon">'.PHP_EOL;
echo '			<img src="../img/png/iconClose.png" width="64" height="64" alt="icon Close Modal" class="modalImage">'.PHP_EOL;
echo '		</div>'.PHP_EOL;
echo '		<div class="textModal center">'.PHP_EOL;
echo '			<p class="capitalize">Dati inseriti correttamente premi ok per proseguire</p>'.PHP_EOL;
echo '		</div>'.PHP_EOL;
echo '		<div class="screenBtn">'.PHP_EOL;
echo '			<div class="col-12-12">'.PHP_EOL;
echo '				<input value="OK" class="uppercase medium no-round buttonSelected no-border" data-button="accept" type="button">'.PHP_EOL;
echo '			</div>'.PHP_EOL;	
echo '		</div>'.PHP_EOL;
echo '	</div>'.PHP_EOL;
echo '</div>'.PHP_EOL;
?>