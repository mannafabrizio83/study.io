<?php
/*---------------------------------INCLUSION-FILE--------------------------------*/
	  
	  	include $_SERVER['DOCUMENT_ROOT'] . '/pages/config/config_db.php';
	  	include $_SERVER['DOCUMENT_ROOT'] . '/pages/function/my_function.php';


/*------------------------------------------------------------------------------*/

	$database = new study_io;
	$conn = $database->connect_db();


/*------------------------------------------------------------------------------*/

	//------------------> READ-DATA-<----------------------
		
		$physics = empty($_POST['physics']) ? 0 : $_POST['physics'];
		$maths = empty($_POST['maths']) ? 0 : $_POST['maths'];
		$chemistry = empty($_POST['chemistry']) ? 0 : $_POST['chemistry'];
		$biology = empty($_POST['biology']) ? 0 : $_POST['biology'];
		$logic = empty($_POST['logic'])  ? 0 : $_POST['logic'];
		$generalCulture = empty($_POST['generalCulture'])  ? 0 : $_POST['generalCulture'];
		$totRisp = empty( $_POST['nRespForAllQuery'] ) ? 0 : $_POST['nRespForAllQuery'];

		if ( $physics && $maths && $chemistry && $biology && $logic && $generalCulture && $totRisp){
				$database->updateDataSettings($conn, $physics, $maths, $chemistry, $biology, $logic, $generalCulture, $totRisp);
		}

?>