<?php
/*-----------------PORTING-LIBRARY-CLASS-----------------*/
	
	include $_SERVER['DOCUMENT_ROOT'] . '/pages/config/config_db.php';
	include $_SERVER['DOCUMENT_ROOT'] . '/pages/function/my_function.php';

/*--------------------MANAGE-OBJECT----------------------*/

  $study = new study_io;
  $conn = $study->connect_db();
	//print_r($_POST);

/*-------------------------------------------------------*/

  //attrSearch        ----------------->    ( 0 ricerca per codice domanda 1 per testo domanda )
  //search            ----------------->    ( Dati Inviati da textbox di ricerca               )
  //searchMaxNumb     ----------------->    ( Limite massimo per ricerca domanda               )
  //subject           ----------------->    ( Materia codici                                   )   

/*-------------------------------------------------------*/

/*
  ATTRSEARCH ->   --------------------------
                | 1) Codice domanda          |
                | 2) Testo della domanda     |
                  --------------------------
 
  SUBJECT   -->  ---------------------------
               | 0) Tutte                    |
               | 1) Fisica                   |
               | 2) Matematica               |
               | 3) Biologia                 |
               | 4) Chimica                  |
               | 5) Logica Matematica        |
               | 6) Logica Verbale           |
               | 7) Cultura Generale         |
                ----------------------------
*/
/*------------------------VARIABLES------------------------*/

  $regExp = "/^[0-9]+$/";
  $searchMaxNumb = preg_match($regExp, $_POST['searchMaxNumb']) ? strip_tags(trim($_POST['searchMaxNumb']) * 4 ) : 0;
  $search = empty($_POST['search']) ? 0 : strip_tags(trim($_POST['search']));
  $regExp = "/^[0-7]$/";
  $subject = preg_match($regExp, $_POST['subject']) ? strip_tags(trim($_POST['subject'])) : 0;
  $regExp = "/^[1-2]$/";
  $attrSearch = preg_match($regExp, $_POST['attrSearch']) ? $_POST['attrSearch'] : 0;

/*--------------------------------------------------------*/

  if( $search && $attrSearch && $searchMaxNumb || $subject ){
  	$data = $study->readAndSendData($conn, $search, $attrSearch, $searchMaxNumb, $subject);
		//print_r($data);
		echo json_encode($data, true);

  }
  else{
    $resp = "Valori non validi!!!!!!!";
		$data = json_encode($resp);
  }

?>