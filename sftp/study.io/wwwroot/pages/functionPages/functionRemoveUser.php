<?php
/*-------------------------------INCLUSION-FILE-----------------------------*/

	include $_SERVER['DOCUMENT_ROOT'] . '/pages/config/config_db.php';
	include $_SERVER['DOCUMENT_ROOT'] . '/pages/function/my_function.php';

/*----------------------------------------------------------------------------*/
	
	$database = new study_io;
	$conn = $database->connect_db();

/*-------------------------------------------------------*/

$uid = empty($_POST['uid']) ? 0 : $_POST['uid'];
$email = empty($_POST['email']) ? 0 : $_POST['email'];
$sendProcess = empty($_POST['sendProcess']) ? 0 : $_POST['sendProcess'];
if( $uid && $email && $sendProcess ){
	$database->updateRemoveUser($conn, $sendProcess, $uid, $email);
}
else if ( $uid || $email && $sendProcess ) {
	$database->updateRemoveUser($conn, $sendProcess, $uid, $email);
}
?>