<?php
/*---------------------------------INCLUSION-FILE--------------------------------*/
	  
	  	include $_SERVER['DOCUMENT_ROOT'] . '/pages/config/config_db.php';
	  	include $_SERVER['DOCUMENT_ROOT'] . '/pages/function/my_function.php';


/*------------------------------------------------------------------------------*/

	$database = new study_io;
	$conn = $database->connect_db();


/*------------------------------------------------------------------------------*/

	//----------------------> READ-DATA-AND-CHECK <------------------------

		$nameUser = empty($_POST['nameUser']) ? 0 : $_POST['nameUser'];
		//print_r($nameUser);
		$surnameUser = empty($_POST['surnameUser']) ? 0 : $_POST['surnameUser'];
		//print_r($surnameUser);
		$regExp = "/^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,}$/";
		$email = preg_match($regExp, $_POST['eMail']) ? $_POST['eMail'] : "0";
		//print_r($email);
		$regExp = "/^[0-9\-]{11,16}$/";
		$telUser = preg_match($regExp, $_POST['telUser']) ? $_POST['telUser'] : 0;
		//print_r($telUser);
		$admin = empty($_POST['admin']) ? 0 : 1;
		//print_r($admin);
		$textUni = empty($_POST['textUni']) ? 0 : $_POST['textUni']; 
		//print_r($textUni);

/*------------------------------------------------------------------------------*/

		if($database->registration($conn, $email, $admin, $nameUser, $surnameUser, $telUser, $textUni)){
			echo "Inserimento eseguito";
		}


/*------------------------------------------------------------------------------*/
?>