<?php
/*-----------------PORTING-LIBRARY-CLASS-----------------*/
	
	include $_SERVER['DOCUMENT_ROOT'] . '/pages/config/config_db.php';
	include $_SERVER['DOCUMENT_ROOT'] . '/pages/function/my_function.php';
	include $_SERVER['DOCUMENT_ROOT'] . '/pages/function/us_function.php';

/*-------------------------------------------------------*/
	
	$username = empty($_POST['userCode']) ? 0 : $_POST['userCode'];
	$passw = empty($_POST['passw']) ? 0 : $_POST['passw'];

/*-------------------------------------------------------*/

	$login = new study_io;
	$conn = $login->connect_db();
	$login->controllUserAccess($conn, $username, $passw);
?>