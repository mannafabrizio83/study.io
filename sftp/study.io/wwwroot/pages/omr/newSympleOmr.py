#!/usr/python2.7
###########################################################

### PORTING DELLE LIBRERIE PER LAVORARE A BASSO LIVELLO

###########################################################


import argparse as arg;
from SimpleCV import Image;
import time;

###########################################################

###              TEST DELLE LIBRERIE

###########################################################

#print cv.__version__;

###########################################################

##              READ DATA BY PATH LINUX

###########################################################


data = arg.ArgumentParser() # Contenitore di valori da shell

###########################################################

#    INFORMAZIONI SUL NUMERO DELLE RISPOSTE PER SOMANDA

###########################################################

data.add_argument("-i", "--image", required=True, help="Inserisci il path dell'immagine", metavar="FILE")
data.add_argument("-d", "--data", required=True, help="Inserisci numero di risposte per prova", metavar="Integer Data")
data.add_argument("-f", "--fisica", required=True,help="Inserisci numero di domande per fisica", metavar="Integer Data")
data.add_argument("-m", "--matematica", required=True,help="Inserisci numero di domande per matematica", metavar="Integer Data")
data.add_argument("-c", "--chimica", required=True, help="Inserisci numero di domande di chimica", metavar="Integer Data")
data.add_argument("-b", "--biologia", required=True, help="Inserisci numero di domande di biologia", metavar="Integer Data")
data.add_argument("-l", "--logica", required=True, help="Inserisci numero di domande di logica", metavar="Integer Data")
data.add_argument("-cg", "--cultura-generale", required=True, help="Inserisci numero di domande di cultura generale", metavar="Integer Data")

###########################################################

#           RAGGRUPPAMENTO DEI DATI IN ARRAY

###########################################################

args = vars(data.parse_args())  # Array che contiene tutti i dati della schell che sono stati passati come argomenti

###########################################################

#                       FUNZIONI VARIE

###########################################################

#def show(args):                       #---> Mostra immagine
#    cv.imshow("Test-Image", args)
#    cv.waitKey(0)


###########################################################

#                   INIZIO PROGRAMMA

###########################################################

image = Image(args["image"]);
time.sleep(5);


#print(rgbRectangle)
#show(res)

###########################################################

##              BINARIZZAZIONE DELL'IMMAGINE

###########################################################

#show(res)