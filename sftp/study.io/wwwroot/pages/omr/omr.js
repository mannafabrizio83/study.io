/*
  ########################################################################################
  #                                                                                      #
  #                                                                                      #
  #                           Programma creato da Manna Fabrizio                         #
  #                          Tecnologia utilizzata: Computer Vision                      #
  #                           OMR Per la correzione dei compiti                          #
  #                                                                                      #
  ########################################################################################
*/

/*
      
    PARSE DATA ----------------  ---------------------------------------------------
                               |- 1. Path dell'immagine                             ( S )
                               |- 2. Data ( N° Risposte per Prova )                 ( I )
                               |- 3. Fisica ( Inserisci numero risposte )           ( I )
                               |- 4. Matematica ( Inserisci numero risposte )       ( I )
                               |- 5. Chimica ( Inserisci numero risposte )          ( I )
                               |- 6. Biologia ( Inserisci numero risposte )         ( I )
                               |- 7. Logica ( Inserisci numero risposte )           ( I )
                               |- 8. Cultura generale ( Inserisci numero risposte ) ( I )
                               |- 9. Indirizzo IP                                   ( I )
                                 ---------------------------------------------------
                               
*/


/*
  #########################################################
                Porting delle librerie ( moduli )
  #########################################################
*/
    const cmd = require('node-cmd');                          // -------->  Libreria per i comandi del terminale
    const cv = require('opencv4nodejs');                      // -------->  Libreria per la ( Computer Vision )
    //const argv = require('argv');                           // -------->  Libreria per la lettura dei dati da terminale
    const argv = require('minimist')(process.argv.slice(2));  // -------->  Libreria per la lettura dei dati da terminale
    const replaceString = require('replace-string'); 

//#########################################################

//            Instaurazione delle variabili 

//#########################################################

    const check = argv._;
    const path = typeof check[0] === 'string' ? check[0]  : 0;
    const data = Number.isInteger(check[1]) ? check[1] : 0;
    const physics = Number.isInteger(check[2]) ? check[2] : 0;
    const maths = Number.isInteger(check[3]) ? check[3] : 0;
    const chemistry = Number.isInteger(check[4]) ? check[4] : 0;
    const biology = Number.isInteger(check[5]) ? check[5] : 0;
    const logic = Number.isInteger(check[6]) ? check[6] : 0;
    const generalCulture = Number.isInteger(check[7]) ? check[7] : 0;
    const ip = typeof check[9] === 'string' ? check[9]  : 0;

    //-----------------------------------------------
    //                    Debug
    //-----------------------------------------------
       // console.log(Number.isInteger(check[9]));
    //-----------------------------------------------

//#########################################################

    if ( path && data && physics && maths && chemistry && biology && logic && generalCulture && ip ){
      var img = cv.imread(path);
      cv.imwrite("result");
    }else{
      console.log("Error Data si consiglia di controllare i parametri!!");
      console.log("Tentativo di string injections");
      cmd.run('echo "######################################################################################## \n" >> logs/errorLog');
      cmd.run(`echo "I dati mandati sono: ${check} \n" >> logs/errorLog`);
    }

//#########################################################
//                        Fine
//#########################################################