<?php
	function sec_session_start() {
		    $session_name = 'session_id';
		    $secure = false; //'https'
		    $httponly = true; // Questo impedirà ad un javascript di essere in grado di accedere all'id di sessione.
		    ini_set('session.use_only_cookies', 1); // Forza la sessione ad utilizzare solo i cookie.
		    $cookieParams = session_get_cookie_params(); // Legge i parametri correnti relativi ai cookie.
		    session_set_cookie_params($cookieParams["lifetime"], $cookieParams["path"], $cookieParams["domain"], $secure, $httponly); 
		    session_name($session_name);
		    session_start(); // Avvia la sessione php.
		    session_regenerate_id(); // Rigenera la sessione e cancella quella creata in precedenza.
	}

	function data_db($data)
	{
	  	if (strpos($data, '-') == false) // Creo una array dividendo la data
	  		$array = explode("/", $data);
	  	else
	  		$array = explode("-", $data);	  

		  // Riorganizzo gli elementi in stile YYYY/MM/DD o DD/MM/YYYY se la prendo dal db
		  $data_it = $array[2]."-".$array[1]."-".$array[0]; 

		  // Restituisco il valore della data in formato italiano
		  return $data_it; 
	}
?>