<?php
	/*-----------Import-Libraries-Php------------*/
	
		use PHPMailer\PHPMailer\PHPMailer;
		use PHPMailer\PHPMailer\Exception;
		require $_SERVER['DOCUMENT_ROOT'] . '/vendor/autoload.php';

	/*--------Functions-Library-Inizialize------*/

		function emailSendRegister($mailTo, $password, $nickname){
			$mail = new PHPMailer(true);
			try {
    			//$mail->SMTPDebug = 4;												//----> DEBUG                     
    			$mail->isSMTP();                                
    			$mail->Host = 'smtp.gmail.com';  
    			$mail->SMTPAuth = true;                               
    			$mail->Username = 'test.webserver97@gmail.com';                
   				$mail->Password = 'FmDj)/&(0';                           
    			$mail->SMTPSecure = 'tls';                            
    			$mail->Port = 587;                                    
    			$mail->setFrom('test.webserver97@gmail.com', 'Test Webserver');
    			$mail->addAddress($mailTo);
				$mail->isHTML(true);
   				$mail->Subject = 'Iscrizione Utente';
    			$mail->Body    = "Username: " . $nickname . "<br/>" . "Password: " . $password ;
    			$mail->send();
    			// echo 'Message has been sent';
			} 
			catch (Exception $e) {
    			echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
			}
		}

	/*----------Inizialization-Object-----------*/
	
		class study_io {

			public $mysqli;
			//Funzioni private

			//Funzioni pubbliche
        	public function connect_db() {  //Connessione al database   	

        		mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
           		try {
	        		$mysqli = new mysqli(HOST, USER, PASSW, DB);
	        		$mysqli->set_charset("utf8");
	        		$this->myconn = $mysqli;
	        		return $this->myconn; //ritorno la connessione al db
        		}
        		catch(Exception $e) {
					error_log($e->getMessage());
					exit('Error connecting to database');
				}
        	}

	 		public function update_user($database, $from, $email, $password, $nickname) { //FUNZIONE PER FARE L'UPDATE SU UN USER

	 			$arrayletterGen = array(
	        		array("a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","z"),
	        		array("A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","Z")
	        	);
	        	$nickname = "";
	        	$password = "";

	        	try{
		        	for ($i=1;$i<=6;$i++) {
		        		$randGen = rand(0 , 1);
		        		if ($randGen === 0) {
		        			$randNumber = rand(0, 9);
		        			$nickname .= $randNumber; 
		        		}
		        		else if ($randGen === 1){
		        			$randRig = rand(0 , 1);
		        			$randColums = rand(0 , 22);
		        			$nickname .= $arrayletterGen[$randRig][$randColums];	
		        		}
		        	}

		        	for ($i=1;$i<=8;$i++) {
		        		$randGen = rand(0 , 1);
		        		if ($randGen === 0) {
		        			$randNumber = rand(0, 9);
		        			$password .= $randNumber; 
		        		}
		        		else if ($randGen === 1){
		        			$randRig = rand(0 , 1);
		        			$randColums = rand(0 , 22);
		        			$password .= $arrayletterGen[$randRig][$randColums];	
		        		}
		        	}
	        	}
	        	catch(Exception $e){
	        		return error_log($e->getMessage());
	        	}

	        	$h_password = hash('sha512', $password);
				emailSendRegister($email, $password, $nickname); //errore

	 			try {
		 			$query = $database->prepare("UPDATE user SET email = ? , password = ?, nickname = ? WHERE coduser = ? LIMIT 1");
					$query->bind_param("sss", $email, $h_password , $nickname);
					$query->execute();
					$query->close();
					return $result;
				} 
				catch(Exception $e) {
					error_log($e->getMessage());
				}
	 		}


 			//FUNZIONE PER LA REGISTRAZIONE
	        public function registration($database, $email, $admin, $nome, $cognome, $tel, $uni) {
	        	$arrayletterGen = array(
	        		array("a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","z"),
	        		array("A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","Z")
	        	);
	        	$nickname = "";
	        	$password = "";

	        	try{
		        	for ($i=1;$i<=6;$i++) {
		        		$randGen = rand(0 , 1);
		        		if ($randGen === 0) {
		        			$randNumber = rand(0, 9);
		        			$nickname .= $randNumber; 
		        		}
		        		else if ($randGen === 1){
		        			$randRig = rand(0 , 1);
		        			$randColums = rand(0 , 22);
		        			$nickname .= $arrayletterGen[$randRig][$randColums];	
		        		}
		        	}

		        	for ($i=1;$i<=8;$i++) {
		        		$randGen = rand(0 , 1);
		        		if ($randGen === 0) {
		        			$randNumber = rand(0, 9);
		        			$password .= $randNumber; 
		        		}
		        		else if ($randGen === 1){
		        			$randRig = rand(0 , 1);
		        			$randColums = rand(0 , 22);
		        			$password .= $arrayletterGen[$randRig][$randColums];	
		        		}
		        	}
	        	}
	        	catch(Exception $e){
	        		return error_log($e->getMessage());
	        	}

	        	$h_password = hash('sha512', $password);
	        	$data = date("Y-m-d");
				emailSendRegister($email, $password, $nickname); //errore

	 			try {
	 				$query = $database->prepare("INSERT INTO user ( email, password, nickname, nome, cognome, tel, data_reg, admin, textuni) VALUES (?, ?, ?, ?, ?, ?, ? ,?, ? )");

					$query->bind_param("sssssssss", $email, $h_password , $nickname, $nome, $cognome, $tel, $data, $admin, $uni);
					$query->execute();
					$query->close();
				} 
				catch(Exception $e) {
					error_log($e->getMessage());
	  				if($database->errno === 1062) {
	  					echo 'Impossibile registrarsi, questo utente esiste già';
	  				}
	  				else {
	  					echo 'Impossibile registrarsi, il database non risponde';
	  				}
				}
	 		}
 
	 		public function select_all($database, $from) { //FUNZIONE PER SELEZIONARE TUTTI GLI ELEMENTI DI UNA TABELLA

	 			try {
		 			$query = $database->prepare("SELECT * FROM $from ");
					$query->execute();
					$result = $query->get_result();
					$query->close();
					return $result;
				} 
				catch(Exception $e) {
					error_log($e->getMessage());
				}
	 		}

	 		public function select_all_id($database, $from, $campo ,$id) { //FUNZIONE PER SELEZIONARE TUTTI GLI ELEMENTI DI UNA TABELLA CON UN CERTO CAMPO ID IN CHIAVE ESTERNA

	 			try {
		 			$query = $database->prepare("SELECT * FROM $from WHERE $campo = ? ");
		 			$query->bind_param("i", $id);
					$query->execute();
					$result = $query->get_result();
					$query->close();
					return $result;
				} 
				catch(Exception $e) {
					error_log($e->getMessage());
				}
	 		}

	 		public function select_id($database, $campo_out, $from, $campo ,$id) { //FUNZIONE PER SELEZIONARE TUTTI GLI ELEMENTI DI UNA TABELLA CON UN CERTO CAMPO ID IN CHIAVE ESTERNA

	 			try {
		 			$query = $database->prepare("SELECT $campo_out FROM $from WHERE $campo = ? ");
		 			$query->bind_param("i", $id);
					$query->execute();
					$result = $query->get_result();
					$query->close();
					return $result;
				} 
				catch(Exception $e) {
					error_log($e->getMessage());
				}
	 		}

	 		public function select_all_id_world($database, $from, $campo ,$id, $campo2, $word) { //FUNZIONE PER SELEZIONARE TUTTI GLI ELEMENTI DI UNA TABELLA CON UN CERTO CAMPO ID IN CHIAVE ESTERNA E CONTENENTE UNA DATA PAROLA

	 			try {
		 			$query = $database->prepare("SELECT * FROM $from WHERE $campo = ? AND $campo2 LIKE ? ");
		 			$word = "%$word%";
		 			$query->bind_param("is", $id, $word);
					$query->execute();
					$result = $query->get_result();
					$query->close();
					return $result;
				} 
				catch(Exception $e) {
					error_log($e->getMessage());
	  				echo 'Contattare un amministratore, Errore:' . $e;
				}
	 		}

	 		public function add_q($database, $codtipodomanda ,$domanda, $giusta) { //FUNZIONE PER AGGIUNGERE DOMANDA

	 			try {
	 				$query = $database->prepare("INSERT INTO domande (codtipodomanda, domanda, giusta) VALUES (?, ?, ?)");

					$query->bind_param("iss", $codtipodomanda, $domanda, $giusta);
					$query->execute();
					$query->close();
				} 
				catch(Exception $e) {
					error_log($e->getMessage());
	  				if($database->errno === 1062) 
	  					echo 'Impossibile aggiungere, questa domanda esiste già';
	  				else 
	  					echo 'Impossibile aggiungere una domanda, il database non risponde';
				}
	 		}

	 		public function add_university($database, $universita) { //FUNZIONE PER AGGIUNGERE DOMANDA

	 			try {
	 				$query = $database->prepare("INSERT INTO universita (nome) VALUES (?)");

					$query->bind_param("s", $universita);
					$query->execute();
					$query->close();
				} 
				catch(Exception $e) {
					error_log($e->getMessage());
	  				if($database->errno === 1062) 
	  					echo 'Impossibile aggiungere, questo corso esiste già';
	  				else 
	  					echo 'Impossibile aggiungere una domanda, il database non risponde';
				}
	 		}

	 		public function admin($database, $admin, $id) { //FUNZIONE PER DARE L'ADMIN AD UN USER

	 			try {
	 				$query = $database->prepare("UPDATE user SET admin = ? WHERE coduser = ?");

					$query->bind_param("ii", $admin, $id);
					$query->execute();
					$query->close();
				} 
				catch(Exception $e) {
					error_log($e->getMessage());
	  				echo 'Impossibile aggiungere una domanda, il database non risponde';
				}
	 		}

	 		public function add_r($database, $coddomanda, $risp) { //FUNZIONE PER AGGIUNGERE UNA RISPOSTA

	 			try {
	 				$query = $database->prepare("INSERT INTO risposte (coddomanda, risposta) VALUES (?, ?)");

					$query->bind_param("is", $coddomanda, $risp);
					$query->execute();
					$query->close();
				} 
				catch(Exception $e) {
					error_log($e->getMessage());
	  				echo 'Impossibile aggiungere una domanda, il database non risponde';
				}
	 		}

	 		public function add_t_e($database, $nome) { //FUNZIONE PER AGGIUNGERE UN TIPO DI ESAME

	 			try {
	 				$query = $database->prepare("INSERT INTO tipoesame (nome) VALUES (?)");

					$query->bind_param("s", $nome);
					$query->execute();
					$query->close();
				} 
				catch(Exception $e) {
					error_log($e->getMessage());
	  				echo 'Impossibile aggiungere un tipo di esame, il database non risponde';
				}
	 		}

	 		public function add_t_d($database, $nome) { //FUNZIONE PER AGGIUNGERE UN TIPO DI DOMANDA

	 			try {
	 				$query = $database->prepare("INSERT INTO tipodomanda (nome) VALUES (?)");

					$query->bind_param("s", $nome);
					$query->execute();
					$query->close();
				} 
				catch(Exception $e) {
					error_log($e->getMessage());
	  				echo 'Impossibile aggiungere un tipo di domanda, il database non risponde';
				}
	 		}

	 		public function delete($database , $from, $campo ,$id) { //FUNZIONE PER ELIMINARE UN ELEMENTO DALLA TABELLA 

	 			try {
	 				$query = $database->prepare("DELETE FROM $from WHERE $campo = $id");
					$query->execute();
					$query->close();
				} 
				catch(Exception $e) {
					error_log($e->getMessage());
	  				echo 'Impossibile eliminare, il database non risponde';
				}
	 		}

	 		public function checkbrute($database, $user_id) {
			   // Recupero il timestamp
			   $now = time();
			   // Vengono analizzati tutti i tentativi di login a partire dalle ultime due ore.
			   $valid_attempts = $now - (2 * 60 * 60); 

			   try {
			   		$brute = $database->prepare("SELECT time FROM login_attempts WHERE coduser = ? AND time > '$valid_attempts'");
			   		$brute->bind_param('i', $user_id); 
			      	// Eseguo la query creata.
			      	$brute->execute();
			      	$brute->store_result();
			      	// Verifico l'esistenza di più di 5 tentativi di login falliti.
			      	if($brute->num_rows > 10) {
			         return true;
			      	} else {
			         return false;
			      	}
			   	}
			   	catch(Exception $e) {
					error_log($e->getMessage());
	  				echo 'Impossibile loggarsi, verificare password o contattare l\'amministratore';
				}
			}

	 		public function login($database, $em_or_nick, $password) {

			   try {
			  
			   		$query = $database->prepare("SELECT * FROM user WHERE email = ? OR nickname = ? LIMIT 1");
				    $query->bind_param('s', $em_or_nick);
				    $query->execute(); // esegue la query appena creata.
					$result = $query->get_result();

					if($result->num_rows > 0)// se l'utente esiste
					         // verificho che non sia disabilitato in seguito all'esecuzione di troppi tentativi di accesso errati.
					{
						$ris = $result->fetch_object();

					    if($this->checkbrute($database, $ris->coduser)) { 
					       // Account disabilitato
					       // Invia un e-mail all'utente avvisandolo che il suo account è stato disabilitato.
					        return false;
					    } 
					    else 
					    {
					    	if(password_verify($password, $ris->password))
					    	{ // Verifica che la password memorizzata nel database corrisponda alla password fornita dall'utente.
					            // Password corretta!            
					               $user_browser = $_SERVER['HTTP_USER_AGENT']; // Recupero il parametro 'user-agent' relativo all'utente corrente.
					 
					               $user_id = preg_replace("/[^0-9]+/", "", $ris->coduser); // ci proteggiamo da un attacco XSS
					               $_SESSION['user_id'] = $user_id; 
					               $nickname = preg_replace("/[^a-zA-Z0-9_\-]+/", "", $ris->nickname); // ci proteggiamo da un attacco XSS
					               $_SESSION['username'] = $nickname;
					               $_SESSION['login_string'] = password_hash($ris->password.$user_browser, PASSWORD_DEFAULT);
					               $query->close();
					               // Login eseguito con successo.
					               return true;    
					        } 
					        else 
					        {
					            // Password incorretta.
					            // Registriamo il tentativo fallito nel database.
					            $now = time();
					            $database->query("INSERT INTO login_attempts (coduser, time) VALUES ('$ris->coduser', '$now')");
					            $query->close();
					            return false;
					        }
					    }
					} 
					else 
					{
					    // L'utente inserito non esiste.
					    $query->close();
					    return false;
					}
			  	}
			  	catch(Exception $e) {
					error_log($e->getMessage());
	  				echo 'Impossibile loggarsi, verificare password o contattare l\'amministratore';
				}
			}

			public function logincheck($database) {
				try{

					// Verifica che tutte le variabili di sessione siano impostate correttamente
				   	if(isset($_SESSION['user_id'], $_SESSION['username'], $_SESSION['login_string']))
				   	{
				   		$user_id = $_SESSION['user_id'];
					    $login_string = $_SESSION['login_string'];
					    $username = $_SESSION['username'];     
					   	$user_browser = $_SERVER['HTTP_USER_AGENT']; // reperisce la stringa 'user-agent' dell'utente.;

					    $query = $database->prepare("SELECT password FROM user WHERE coduser = ? LIMIT 1");
					    $query->bind_param('i', $user_id); // esegue il bind del parametro '$user_id'.
					    $query->execute(); // Esegue la query creata.
					    $query->store_result();

					    if($query->num_rows == 1)
					    { // se l'utente esiste

					   		$query->bind_result($password);
					   		$query->fetch();

					        if(password_verify($password.$user_browser, $login_string)){
					              // Login eseguito!!!!
					        	$query->close();
					            return true;
					        }
					        else 
					        {
					            //  Login non eseguito
					            $query->close();
					            return false;
					        }
					    }
					    else
					    {
					        // Login non eseguito
					        $query->close();
					        return false;
					    }
					}
		 			else 
		 			{
				     	// Login non eseguito
				     	return false;
				   	}		

				}
				catch(Exception $e) {
					error_log($e->getMessage());
	  				echo 'Impossibile loggarsi, verificare password o contattare l\'amministratore';
				}
			   	
			}

			public function controllUserAccess($conn, $nickname, $password ){
				$this->nickname = isset($nickname) ? $nickname : 0;
				$this->password = isset($password) ? hash('sha512', $password) : 0;
				// echo $this->password;
				switch ($this->nickname || $this->password) {
				  	case 1:
				  		//echo "Correct Data Inserted";
				  		$query = $conn->prepare("SELECT nickname, password, admin,nome FROM user WHERE nickname = ? LIMIT 1");
				  		$query->bind_param("s", $this->nickname);
				  		$query->execute();
				  		$query->bind_result($nicks, $pass, $adm, $name);
				  		$query->fetch();
				  		// echo  $this->password ."<br>";
				  		// echo $pass; 
				  		if ( ($nicks === $this->nickname) && ( $pass === $this->password ) ) {
				  			session_start();
				  			$_SESSION['nickname'] = $this->nickname;
				  			$_SESSION['password'] = $this->password;
				  			$_SESSION['name'] = $name;
				  			$conn->close();
				  			header("location: /pages/pannell.php");
				  		}
              else{
                header("location:/login.php?3471EF91CF70DDC937BEF8BCB6DF59886E016E103328B3723C09094D28A4B09D=1");
              }
				  		// echo "Data Passed";
				  		//print_r($query->store_result());
				  		break;
				  	
				  	default:
				  		//echo "Data Incorrect Inserted"; 
				  		return 0;
				  		break;
				  }  
			}

			public function insertAccess($conn, $ip, $hour, $nickname, $data){
				$query = $conn->prepare("INSERT INTO accesso ( data, time_connections, ip, nickname ) VALUES ( ?, ?, ?, ? )");
				$query->bind_param("ssss", $data, $hour, $ip, $nickname);
				$query->execute();
				$conn->close();
			}

			public function logincheck_a($database) {
				try{

					// Verifica che tutte le variabili di sessione siano impostate correttamente
				   	if(isset($_SESSION['user_id'], $_SESSION['username'], $_SESSION['login_string']))
				   	{
				   		$user_id = $_SESSION['user_id'];
					    $login_string = $_SESSION['login_string'];
					    $username = $_SESSION['username'];     
					   	$user_browser = $_SERVER['HTTP_USER_AGENT']; // reperisce la stringa 'user-agent' dell'utente.;

					    $query = $database->prepare("SELECT password FROM user WHERE coduser = ? AND admin = 1 LIMIT 1");
					    $query->bind_param('i', $user_id); // esegue il bind del parametro '$user_id'.
					    $query->execute(); // Esegue la query creata.
					    $query->store_result();

					    if($query->num_rows == 1)
					    { // se l'utente esiste

					   		$query->bind_result($password);
					   		$query->fetch();

					        if(password_verify($password.$user_browser, $login_string)){
					              // Login eseguito!!!!
					        	$query->close();
					            return true;
					        }
					        else 
					        {
					            //  Login non eseguito
					            $query->close();
					            return false;
					        }
					    }
					    else
					    {
					        // Login non eseguito
					        $query->close();
					        return false;
					    }
					}
		 			else 
		 			{
				     	// Login non eseguito
				     	return false;
				   	}		

				}
				catch(Exception $e) {
					error_log($e->getMessage());
	  				echo 'Impossibile loggarsi, verificare password o contattare l\'amministratore';
				}
			   	
			}

			/*Settings Form*/
			public function printSettings($database){
				$query = $database->query("SELECT fisica, matematica, chimica,biologia, logica, cultura_generale, tot_risposte FROM settings LIMIT 1");
				$result = $query->fetch_assoc();
				// print_r($result);
				return $result;
				$database->close();
			}

			public function updateDataSettings($database, $physics, $maths, $chemistry, $biology, $logic, $generalCulture, $totRisp){
				$query = $database->prepare("UPDATE settings SET fisica = ? , matematica = ?, chimica = ?, biologia = ?, logica = ?, cultura_generale = ?, tot_risposte = ? WHERE id_settings = 1");
				$query->bind_param("iiiiiii", $physics, $maths, $chemistry, $biology, $logic, $generalCulture, $totRisp);
				$query->execute();
				$query->close();
			} 


			public function printUsers($database){
				$query = $database->query("SELECT coduser, nome, cognome, email FROM user");
				//$result = $query->store_result();
				// print_r($result);
				$result = $query->fetch_all(MYSQLI_ASSOC);
				return $result;
				//$database->close();
				$query->close();

			}

			public function updateRemoveUser($database, $sendProcess, $uid, $email){
				if( $sendProcess == 1){
					$arrayletterGen = array(
	        		array("a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","z"),
	        		array("A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","Z")
	        	);
		        	$nickname = "";
		        	$password = "";

		        	try{
			        	for ($i=1;$i<=6;$i++) {
			        		$randGen = rand(0 , 1);
			        		if ($randGen === 0) {
			        			$randNumber = rand(0, 9);
			        			$nickname .= $randNumber; 
			        		}
			        		else if ($randGen === 1){
			        			$randRig = rand(0 , 1);
			        			$randColums = rand(0 , 22);
			        			$nickname .= $arrayletterGen[$randRig][$randColums];	
			        		}
			        	}

			        	for ($i=1;$i<=8;$i++) {
			        		$randGen = rand(0 , 1);
			        		if ($randGen === 0) {
			        			$randNumber = rand(0, 9);
			        			$password .= $randNumber; 
			        		}
			        		else if ($randGen === 1){
			        			$randRig = rand(0 , 1);
			        			$randColums = rand(0 , 22);
			        			$password .= $arrayletterGen[$randRig][$randColums];	
			        		}
			        	}
		        	}
		        	catch(Exception $e){
		        		return error_log($e->getMessage());
		        	}

		        	$h_password = hash('sha512', $password);
		        	$data = date("Y-m-d");
					emailSendRegister($email, $password, $nickname); //errore
					$query = $database->prepare("UPDATE user SET nickname = ? , password = ? WHERE coduser = ?");
					$query->bind_param("sss", $nickname, $h_password, $uid);
					$query->execute();
					$query->close();
				}
				else if( $sendProcess == 2){
					$query = $database->prepare("DELETE FROM user WHERE coduser = ?");
					$query->bind_param("s", $uid);
					$query->execute();
					$query->close();
				}
			} 

			public function createQuery($database){
				$query = $database->query("SELECT tot_risposte FROM settings");
				$result = $query->fetch_all(MYSQLI_ASSOC);
				return $result;
				$query->close();

			}

			public function printSubject($database){
				$query = $database->query("SELECT codtipodomanda, nome FROM tipodomanda");
				$result = $query->fetch_all(MYSQLI_ASSOC);
				return $result;
				$query->close();
			}

			public function createQueryImage($database, $requestNumber, $rand, $codType, $queryes, $resp, $respErr, $pathImg){
				switch ($requestNumber) {
					case 0:
						$query = $database->prepare("INSERT INTO domande (coddomanda, codtipodomanda, domanda, giusta, path_img) VALUES ( ?, ?, ?, ?, ? )");
						$query->bind_param( "iisss", $rand, $codType, $queryes, $resp,  $pathImg );
						$query->execute();
						for ($i=1; $i <= sizeof($respErr); $i++){
							$query = $database->prepare("INSERT INTO risposte (coddomanda, risposta) VALUES ( ?, ? )");
							$query->bind_param( "is", $rand, $respErr[$i]);
							$query->execute();
						}
						$query->close();
						break;
					case 1:
						$query = $database->prepare("INSERT INTO domande (coddomanda, codtipodomanda, domanda, giusta) VALUES ( ?, ?, ?, ? )");
						$query->bind_param( "iiss", $rand, $codType, $queryes, $resp );
						$query->execute();
						for ($i=1; $i <= sizeof($respErr); $i++){
							$query = $database->prepare("INSERT INTO risposte (coddomanda, risposta) VALUES ( ?, ? )");
							$query->bind_param( "is", $rand, $respErr[$i]);
							$query->execute();
						}
						$query->close();
					default:
						return 0;
						break;
				}
			}
		  
      public function inizializeStampData($database){
        $query = $database->query("SELECT coddomanda,codtipodomanda,domanda,giusta FROM domande LIMIT 25");
				$result = $query->fetch_all(MYSQLI_ASSOC);
				return $result;
				$query->close();
      }
      
      public function readAndSendData($database, $search, $attrSearch, $searchMaxNumb, $subject){
        if($database && $search && $attrSearch && $searchMaxNumb && $subject){
          //echo "Valori ricevuti 1";
					switch ($attrSearch){
						case 1:
							$query = $database->prepare("SELECT tipodomanda.nome, domande.domanda, domande.giusta, risposte.coddomanda FROM domande INNER JOIN risposte ON risposte.coddomanda = domande.coddomanda INNER JOIN tipodomanda ON tipodomanda.codtipodomanda = ? WHERE domande.coddomanda LIKE CONCAT('%',?,'%') LIMIT ?");
							$query->bind_param( "iii", $subject, $search, $searchMaxNumb );
							$query->execute();
							$data = $query->get_result()->fetch_all(MYSQLI_ASSOC);
							return $data;
							break;
						case 2:
							$query = $database->prepare("SELECT tipodomanda.nome, domande.domanda, domande.giusta, risposte.coddomanda FROM domande INNER JOIN risposte ON risposte.coddomanda = domande.coddomanda INNER JOIN tipodomanda ON tipodomanda.codtipodomanda = ? WHERE domande.domanda LIKE CONCAT('%',?,'%') LIMIT ?");
							$query->bind_param( "isi", $subject, $search, $searchMaxNumb );
							$query->execute();
							//$query->bind_result( $nameTypeQuery, $queryQuest, $queryCorrect, $errorQuery );
							$data = $query->get_result()->fetch_all(MYSQLI_ASSOC);
							return $data;
							break;
						default:
							return 0;
					}
        }
				else if($database && $attrSearch && $searchMaxNumb && $search){
          //echo "Valori ricevuti 2";
					switch ($attrSearch){
						case 1:
							$query = $database->prepare("SELECT tipodomanda.nome, domande.domanda, domande.giusta, risposte.risposta FROM domande INNER JOIN tipodomanda ON tipodomanda.codtipodomanda = domande.codtipodomande INNER JOIN risposte ON risposte.coddomanda = domande.coddomanda WHERE domande.coddomanda LIKE CONCAT('%',?,'%') LIMIT ?");
							$query->bind_param( "ii", $search, $searchMaxNumb );
							$query->execute();
							$data = $query->get_result()->fetch_all(MYSQLI_ASSOC);
							return $data;
						case 2:
							$query = $database->prepare("SELECT tipodomanda.nome, domande.domanda, domande.giusta, risposte.coddomanda FROM domande INNER JOIN risposte ON risposte.coddomanda = domande.coddomanda INNER JOIN tipodomanda ON tipodomanda.codtipodomanda = domande.codtipodomanda WHERE domande.domanda LIKE CONCAT('%',?,'%') LIMIT ?");
							$query->bind_param( "si", $search, $searchMaxNumb );
							$query->execute();
							//$query->bind_result( $nameTypeQuery, $queryQuest, $queryCorrect, $errorQuery );
							$data = $query->get_result()->fetch_all(MYSQLI_ASSOC);
							return $data;
						default:
							return 0;
					}
        }
				else if($database && $attrSearch && $searchMaxNumb && $subject){
          //echo "Valori ricevuti 2";
					switch ($attrSearch){
						case 1:
							$query = $database->prepare("SELECT tipodomanda.nome, domande.domanda, domande.giusta, risposte.risposta FROM domande INNER JOIN tipodomanda ON tipodomanda.codtipodomanda = domande.codtipodomanda INNER JOIN risposte ON risposte.coddomanda = domande.coddomanda WHERE domande.codtipodomanda LIKE CONCAT('%',?,'%') LIMIT ?");
							$query->bind_param( "ii", $subject, $searchMaxNumb );
							$query->execute();
							$data = $query->get_result()->fetch_all(MYSQLI_ASSOC);
							return $data;
						case 2:
							$query = $database->prepare("SELECT tipodomanda.nome, domande.domanda, domande.giusta, risposte.coddomanda FROM domande INNER JOIN risposte ON risposte.coddomanda = domande.coddomanda INNER JOIN tipodomanda ON tipodomanda.codtipodomanda = domande.codtipodomanda WHERE domande.codtipodomanda LIKE CONCAT('%',?,'%') LIMIT ?");
							$query->bind_param( "ii", $subject, $searchMaxNumb );
							$query->execute();
							//$query->bind_result( $nameTypeQuery, $queryQuest, $queryCorrect, $errorQuery );
							$data = $query->get_result()->fetch_all(MYSQLI_ASSOC);
							return $data;
						default:
							return 0;
					}
        }
				else if($database && $attrSearch && $searchMaxNumb ){
          //echo "Valori ricevuti 2";
					switch ($attrSearch){
						case 1:
							$query = $database->prepare("SELECT tipodomanda.nome, domande.domanda, domande.giusta, risposte.risposta FROM domande INNER JOIN tipodomanda ON tipodomanda.codtipodomanda = domande.codtipodomanda INNER JOIN risposte ON risposte.coddomanda = domande.coddomanda LIMIT ?");
							$query->bind_param( "i", $searchMaxNumb );
							$query->execute();
							$data = $query->get_result()->fetch_all(MYSQLI_ASSOC);
							return $data;
						case 2:
							$query = $database->prepare("SELECT tipodomanda.nome, domande.domanda, domande.giusta, risposte.coddomanda FROM domande INNER JOIN risposte ON risposte.coddomanda = domande.coddomanda INNER JOIN tipodomanda ON tipodomanda.codtipodomanda = domande.codtipodomanda LIMIT ?");
							$query->bind_param( "i", $searchMaxNumb );
							$query->execute();
							//$query->bind_result( $nameTypeQuery, $queryQuest, $queryCorrect, $errorQuery );
							$data = $query->get_result()->fetch_all(MYSQLI_ASSOC);
							return $data;
						default:
							return 0;
					}
        }
				else{
					switch ($attrSearch){
						case 1:
							$query = $database->prepare("SELECT tipodomanda.nome, domande.domanda, domande.giusta, risposte.risposta FROM domande INNER JOIN tipodomanda ON tipodomanda.codtipodomanda = domande.codtipodomanda INNER JOIN risposte ON risposte.coddomanda = domande.coddomanda LIMIT ?");
							$query->bind_param( "i", $searchMaxNumb );
							$query->execute();
							$data = $query->get_result()->fetch_all(MYSQLI_ASSOC);
							return $data;
						case 2:
							$query = $database->prepare("SELECT tipodomanda.nome, domande.domanda, domande.giusta, risposte.coddomanda FROM domande INNER JOIN risposte ON risposte.coddomanda = domande.coddomanda INNER JOIN tipodomanda ON tipodomanda.codtipodomanda = domande.codtipodomanda LIMIT ?");
							$query->bind_param( "i", $searchMaxNumb );
							$query->execute();
							//$query->bind_result( $nameTypeQuery, $queryQuest, $queryCorrect, $errorQuery );
							$data = $query->get_result()->fetch_all(MYSQLI_ASSOC);
							return $data;
						default:
							return 0;
					}
				}
      }
    }
?>