<?php	  
	  	include $_SERVER['DOCUMENT_ROOT'] . '/pages/config/config_db.php';
	  	include $_SERVER['DOCUMENT_ROOT'] . '/pages/function/my_function.php';
	  	include $_SERVER['DOCUMENT_ROOT'] . '/pages/function/us_function.php';

	  	$database = new study_io();
		$conn = $database->connect_db();
		session_start();

		$admin = isset($_POST['admin']) ? $_POST['admin'] : '0';

		if(isset($_POST['university']))
		{
			$iduni = $_POST['university'];
		}
		
		if(isset($_POST['textUni']))
		{ 	
			$university = $_POST['textUni'];
			$database->add_university($conn, $_POST['textUni']);

			$iduni = select_id($conn, "coduni", "universita", "nome" , $university);
		}

		$database->registration($conn, $_POST['idCard'], $_POST['eMail'], $admin, $_POST['u_password'], $_POST['nameUser'], $_POST['surname'], $_POST['telUser'], $_POST['dateOfBirth'], $iduni);

		header('Location: ../pannel.html');
		exit();
?>