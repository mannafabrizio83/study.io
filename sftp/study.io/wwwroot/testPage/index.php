<?php
echo '<!DOCTYPE html>'.PHP_EOL;
echo '<html lang="it" ng-app="myapp">'.PHP_EOL;
echo '<head>'.PHP_EOL;
echo '	<title>Sito In Manutenzione</title>'.PHP_EOL;
echo '	<meta charset="UTF-8">'.PHP_EOL;
echo '	<meta name="viewport" content="width=device-width, initial-scale=1.0">'.PHP_EOL;
echo '	<meta name="description" content="Sito in manutenzione">'.PHP_EOL;
echo '	<meta name="robots" content="noindex,nofollow">'.PHP_EOL;
echo '	<meta name="author" content="Manna Fabrizio , D\'Angelo Vito">'.PHP_EOL;
echo '	<meta http-equiv="x-ua-Compatible" content="IE=edge">'.PHP_EOL;
echo '	<link rel="canonical" href="dev.developer-manna.tk/">'.PHP_EOL;
echo '	<style>'.PHP_EOL;
echo '		h1,p{font-size:2em;text-align:center;text-transform:uppercase;font-family:monospace}'.PHP_EOL;
echo '		.elements-center{position:absolute;top:50%;left:50%;transform:translate(-50%, -50%);-moz-transform:translate(-50%, -50%);-ms-transform:translate(-50%, -50%);-o-transform:translate(-50%, -50%);-webkit-transform:translate(-50%, -50%)}'.PHP_EOL;
echo '	</style>'.PHP_EOL;
echo '</head>'.PHP_EOL;
echo '<body>'.PHP_EOL;
echo '	<div class="elements-center">'.PHP_EOL;
echo '		<h1>Sito in Manutenzione</h1>'.PHP_EOL;
echo '		<p>Si prega di contattare l\'amministratore</p>'.PHP_EOL;
echo '	</div>'.PHP_EOL;
echo '	<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.6/angular.min.js" defer></script>'.PHP_EOL;
echo '	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js" defer></script>'.PHP_EOL;
echo '</body>'.PHP_EOL;
echo '</html>'.PHP_EOL;
?>